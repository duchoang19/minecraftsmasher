"use strict";
cc._RF.push(module, '49cb46jVzNB6KWPjpyr+e3g', 'BloodController');
// Scripts/VFX/BloodController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BloodController = /** @class */ (function (_super) {
    __extends(BloodController, _super);
    function BloodController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BloodController.prototype.onLoad = function () {
        var tween = new cc.Tween().to(0.6, { scale: 500 });
        //tween.target(this.node.children[2]).start();
        tween.target(this.node).start();
    };
    BloodController = __decorate([
        ccclass
    ], BloodController);
    return BloodController;
}(cc.Component));
exports.default = BloodController;

cc._RF.pop();