"use strict";
cc._RF.push(module, '6fb44ar/vdHCqZo6WpsF2GK', 'EnemyController');
// Scripts/Enemy/EnemyController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Random_1 = require("../Common/Random");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnemyController = /** @class */ (function (_super) {
    __extends(EnemyController, _super);
    function EnemyController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampTop = 0;
        _this.clampBottom = 0;
        _this.PointShoot = null;
        _this.bodyDeath = null;
        _this.effectBlood = null;
        _this.posBlood = null;
        _this.moveX = 0;
        _this.moveY = 0;
        _this.moveZ = 0;
        _this.degree = 0;
        _this.boolCheckAttacking = false;
        _this.boolCheckAttacked = false;
        _this.boolEnemyDeath = false;
        _this.checkFollowPlayer = false;
        _this.checkDeath = false;
        return _this;
    }
    EnemyController.prototype.onLoad = function () {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    };
    EnemyController.prototype.update = function () {
        var _this = this;
        if (!this.checkDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
                var maxDistance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolEnemyDeath) {
                            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.getComponent(CharacterController_1.default).LevelUpPlayer();
                            // this.DestroyByKatana();
                            this.scheduleOnce(function () {
                                _this.DestroyByHammer();
                            }, 0.1);
                            this.checkDeath = true;
                            Global_1.default.boolCheckAttacked = false;
                            Global_1.default.boolCheckAttacking = false;
                        }
                    }
                }
            }
        }
    };
    EnemyController.prototype.StartMove = function () {
        var _this = this;
        this.schedule(function () {
            _this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    };
    EnemyController.prototype.DestroyByHammer = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        // this.PointShoot.active = false;
        this.node.eulerAngles = new cc.Vec3(0, 0, this.node.eulerAngles.z);
        // this.node.scaleZ = 100;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 5);
    };
    EnemyController.prototype.DestroyByKatana = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
        this.node.stopAllActions();
        // this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
    };
    EnemyController.prototype.SpawnerBlood = function () {
        this.bodyDeath.active = true;
        var pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 2);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    };
    EnemyController.prototype.SpawnerEffectBlood = function (x, y, z) {
        var EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    };
    EnemyController.prototype.SpawnerBodyDeath = function (timing) {
        var _this = this;
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(function () {
            _this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            _this.node.destroy();
        }, timing);
    };
    EnemyController.prototype.moveEnemy = function () {
        var _this = this;
        if (this.node.name == 'EnemyHunter1') {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hunter_run");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
        }
        if (this.checkFollowPlayer) {
            var Distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
            var duration = Distance / 21;
            this.moveX = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            while (Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    };
    EnemyController.prototype.betweenDegree = function (dirVec, comVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    EnemyController.prototype.EnemyAttack = function () {
        var _this = this;
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(function () {
            _this.boolCheckAttacking = true;
            _this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(function () {
        }, 0.6);
        this.scheduleOnce(function () {
            Global_1.default.boolStartAttacking = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    };
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "PointShoot", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "bodyDeath", void 0);
    __decorate([
        property(cc.Prefab)
    ], EnemyController.prototype, "effectBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "posBlood", void 0);
    EnemyController = __decorate([
        ccclass
    ], EnemyController);
    return EnemyController;
}(cc.Component));
exports.default = EnemyController;

cc._RF.pop();