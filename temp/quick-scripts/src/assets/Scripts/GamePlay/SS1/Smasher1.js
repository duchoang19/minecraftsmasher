"use strict";
cc._RF.push(module, '743adJDcwFAjoO1TT8zhVhi', 'Smasher1');
// Scripts/GamePlay/SS1/Smasher1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Smasher1 = /** @class */ (function (_super) {
    __extends(Smasher1, _super);
    function Smasher1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.btnContinue = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.txtEndGame = null;
        _this.nStart = null;
        _this.countEnemy = 0;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        Smasher1_1._instance = _this;
        return _this;
    }
    Smasher1_1 = Smasher1;
    Smasher1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher1.prototype.start = function () {
        // this.playGame();
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    Smasher1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    Smasher1.prototype.update = function (dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    };
    Smasher1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    Smasher1.prototype.endGame = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.nightEnd.active = true;
        this.btnContinue.active = true;
        this.txtEndGame.active = true;
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.btnDownload.active = false;
    };
    Smasher1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    Smasher1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    Smasher1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var Smasher1_1;
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "btnContinue", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], Smasher1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nEndCard", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "txtEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nStart", void 0);
    Smasher1 = Smasher1_1 = __decorate([
        ccclass
    ], Smasher1);
    return Smasher1;
}(Singleton_1.default));
exports.default = Smasher1;

cc._RF.pop();