"use strict";
cc._RF.push(module, '9d7b6zZ1X1KDK3CFBTDTqHF', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    scale: "scale",
    checkAttacked: "checkAttacked",
    plusEnemy: "plusEnemy",
    plusCamera: "plusCamera",
    activeGuide: "activeGuide"
};
exports.default = KeyEvent;

cc._RF.pop();