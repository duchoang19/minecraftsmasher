
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Camera/CameraFollow');
require('./assets/Scripts/Character/CharacterController');
require('./assets/Scripts/Common/AdManager');
require('./assets/Scripts/Common/EnableEngine');
require('./assets/Scripts/Common/EnumDefine');
require('./assets/Scripts/Common/GamePlayInstance');
require('./assets/Scripts/Common/Global');
require('./assets/Scripts/Common/KeyEvent');
require('./assets/Scripts/Common/PlatformBtn');
require('./assets/Scripts/Common/Random');
require('./assets/Scripts/Common/Singleton');
require('./assets/Scripts/Common/SoundManager');
require('./assets/Scripts/Common/Utility');
require('./assets/Scripts/Enemy/EnemyController');
require('./assets/Scripts/GamePlay/MC1/MineCraft1');
require('./assets/Scripts/GamePlay/MC2/MineCraft2');
require('./assets/Scripts/GamePlay/SS1/Smasher1');
require('./assets/Scripts/GamePlay/SS2/Smasher2');
require('./assets/Scripts/Joystick/JoystickFollow');
require('./assets/Scripts/VFX/BloodController');
require('./assets/Scripts/VFX/DestroyVFX');
require('./assets/Scripts/Vent/VentCtrl');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/MC1/MineCraft1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '07cd3jvYVVF2L0lnqGm6NoC', 'MineCraft1');
// Scripts/GamePlay/MC1/MineCraft1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../../Character/CharacterController");
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft1 = /** @class */ (function (_super) {
    __extends(MineCraft1, _super);
    function MineCraft1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.btnContinue = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.nStart = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.nEnemyEnd = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1._instance = _this;
        return _this;
    }
    MineCraft1_1 = MineCraft1;
    MineCraft1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.start = function () {
        // this.playGame();
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    MineCraft1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    MineCraft1.prototype.update = function (dt) {
        var _this = this;
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    MineCraft1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft1.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    // this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    MineCraft1.prototype.EndGame = function () {
        var _this = this;
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.nEndCard.active = true;
        this.playGame();
        this.Map.active = true;
        this.nEndCard.children[2].active = true;
        this.nEndCard.children[3].active = true;
        this.nEndCard.children[4].active = true;
        for (var i = 0; i < this.nEnemyEnd.childrenCount; i++) {
            this.nEnemyEnd.children[i].getComponent(EnemyController_1.default).StartMove();
        }
        this.MyCharacter.x = 0;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 12;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 0);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.scheduleOnce(function () {
            _this.nEndCard.children[3].active = false;
        }, 5);
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var MineCraft1_1;
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnContinue", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEndCard", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nStart", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEnemyEnd", void 0);
    MineCraft1 = MineCraft1_1 = __decorate([
        ccclass
    ], MineCraft1);
    return MineCraft1;
}(Singleton_1.default));
exports.default = MineCraft1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXE1DMVxcTWluZUNyYWZ0MS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwyRUFBc0U7QUFDdEUsa0VBQXlEO0FBQ3pELDhDQUF5QztBQUN6QyxrREFBNkM7QUFDN0Msb0RBQStDO0FBQy9DLCtEQUEwRDtBQUVwRCxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXdDLDhCQUFxQjtJQTBEekQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUExREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFMUIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFDdkIsa0JBQVksR0FBWSxLQUFLLENBQUM7UUFFOUIsMEJBQW9CLEdBQVksS0FBSyxDQUFDO1FBQ3RDLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUlwQixZQUFVLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDaEMsQ0FBQzttQkE3RGdCLFVBQVU7SUErRDNCLDZCQUFRLEdBQVI7UUFDSSwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RCwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQ0ksbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxtQ0FBYyxHQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsMkJBQU0sR0FBTixVQUFRLEVBQUU7UUFBVixpQkFRQztRQVBHLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUM3RCxnQkFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDO0lBRUQsNkJBQVEsR0FBUjtRQUNJLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCwrQkFBVSxHQUFWO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM5QixnQ0FBZ0M7b0JBQ2hDLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCw0QkFBTyxHQUFQO1FBQUEsaUJBbUNDO1FBbENHLGdCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEMsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDeEU7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDakYsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BGLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFBQSxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzdDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxnQ0FBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7O0lBbExEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7c0RBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNTO0lBRzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpREFDUTtJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1E7SUFoRFQsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQXNMOUI7SUFBRCxpQkFBQztDQXRMRCxBQXNMQyxDQXRMdUMsbUJBQVMsR0FzTGhEO2tCQXRMb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi8uLi9DaGFyYWN0ZXIvQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi8uLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi8uLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uLy4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNaW5lQ3JhZnQxIGV4dGVuZHMgU2luZ2xldG9uPE1pbmVDcmFmdDE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5pZ2h0RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkNvbnRpbnVlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFNtYXNoZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5lbXlQYXJlbnQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuRG93bmxvYWQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbkVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgblN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGNpcmNsZUVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5BbGw6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbkVuZW15RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBNaW5lQ3JhZnQxLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICAvLyB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGlja0J0blBsYXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5uU3RhcnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlN1cHBvcnRFbmQoKTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBsYXlHYW1lKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5nZXRDb21wb25lbnQoRW5lbXlDb250cm9sbGVyKS5TdGFydE1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3VwcG9ydEVuZCgpIHtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubmlnaHRFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5pZ2h0RW5kLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuTXlDaGFyYWN0ZXIub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5FbmRHYW1lKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAwLjYpO1xyXG4gICAgICAgICAgICB9LCAwLjQpO1xyXG4gICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH0sIDAuMik7XHJcbiAgICB9XHJcblxyXG4gICAgRW5kR2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbGVuZEcgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5uRW5kQ2FyZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMucGxheUdhbWUoKTtcclxuICAgICAgICB0aGlzLk1hcC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubkVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm5FbmRDYXJkLmNoaWxkcmVuWzNdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5uRW5kQ2FyZC5jaGlsZHJlbls0XS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLm5FbmVteUVuZC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5uRW5lbXlFbmQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIueCA9IDA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci55ID0gLTYwO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuc2NhbGUgPSAxMjtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoOTAsIDAsIDApO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuYm9keURlYXRoLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmFjdGl2ZSA9IGZhbHNlOztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5idG5BbGwuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfSwgNSlcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Character/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8c229TU8nVEg7patjhCarLi', 'CharacterController');
// Scripts/Character/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.JoystickFollow = null;
        _this.actionType = EnumDefine_1.ActionType.IDLE;
        _this.speed = 250;
        _this.Weapon = null;
        _this.timeAnim = 0;
        _this.placeBua = null;
        _this.effectSmoke = null;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampBottom = 0;
        _this.clampTop = 0;
        _this.level = 0;
        _this.scale = 0;
        _this.originalSpeed = 0;
        _this.attack = false;
        _this.gameplayInstance = null;
        _this.boolPlaySoundFoot = false;
        return _this;
    }
    // onLoad () {}
    CharacterController.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        Global_1.default.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var physic_mamanger = cc.director.getPhysics3DManager();
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
        physic_mamanger.enabled = true;
        physic_mamanger.gravity = cc.v3(0, 0, -3000);
        this.collider = this.getComponent(cc.Collider3D);
    };
    CharacterController.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !Global_1.default.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            var PosForX = this.node.getPosition();
            var PosForY = this.node.getPosition();
            PosForX.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
                this.scheduleOnce(function () {
                    _this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global_1.default.touchPos.y, Global_1.default.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        Global_1.default.boolStartAttacking = true;
        Global_1.default.boolCheckAttacking = false;
        // this.Hand.active = true;
        this.Weapon.active = true;
        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_attack");
        }
        // if(!Global.teleport) {
        //     cc.log('=======' , this.attack)
        //     this.attack = true;
        // } else {
        //     this.attack = false;
        // }
        this.scheduleOnce(function () {
            Global_1.default.boolCheckAttacking = true;
            Global_1.default.boolCheckAttacked = true;
            // this.spawnEffectSmoke(this.effectSmoke);
            if (_this.node.name == "MyDeadpool") {
                cc.audioEngine.playEffect(Global_1.default.katanaAttack, false);
            }
            else {
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
            }
        }, 0.5);
        // if(Global.teleport) {
        //     this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        // }
        this.scheduleOnce(function () {
            GamePlayInstance_1.instance.emit(KeyEvent_1.default.activeGuide);
            Global_1.default.boolStartAttacking = false;
            if (_this.node.name == "MyDeadpool") {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_idle");
            }
            else {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            }
            // this.Hand.active = false;
            // this.Weapon.active = false;
        }, this.timeAnim);
    };
    CharacterController.prototype.LevelUpPlayer = function () {
        this.node.scaleX = this.node.scaleX + this.scale;
        this.node.scaleY = this.node.scaleY + this.scale;
        this.node.scaleZ = this.node.scaleZ + this.scale;
        if (this.level < 4)
            this.level++;
    };
    CharacterController.prototype.spawnEffectSmoke = function (smoke) {
        var smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeBua.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "JoystickFollow", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.ActionType) })
    ], CharacterController.prototype, "actionType", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speed", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "Weapon", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeBua", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "effectSmoke", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "level", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "scale", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2hhcmFjdGVyXFxDaGFyYWN0ZXJDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG1EQUFrRDtBQUNsRCwrREFBd0U7QUFDeEUsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUVwQyxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBRzFDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBZ0tDO1FBNUpHLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLGdCQUFVLEdBQWUsdUJBQVUsQ0FBQyxJQUFJLENBQUM7UUFHekMsV0FBSyxHQUFXLEdBQUcsQ0FBQztRQUdwQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixpQkFBVyxHQUFjLElBQUksQ0FBQztRQUc5QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBR3RCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBR3ZCLGlCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBR3hCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUdsQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBS2xCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFFeEIsc0JBQWdCLEdBQXFCLElBQUksQ0FBQztRQUMxQyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7O0lBNEd2QyxDQUFDO0lBMUdHLGVBQWU7SUFFZixtQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BFLGdCQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RELGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO1FBQzlCLGVBQWUsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsb0NBQU0sR0FBTjtRQUFBLGlCQXVCQztRQXRCRyxJQUFHLGdCQUFNLENBQUMsZUFBZSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUNyRCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzRSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3RDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDdEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztnQkFDbkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ1g7WUFDRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RCxJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pDLE1BQU0sR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUN6RDtJQUNMLENBQUM7SUFFRCx1Q0FBUyxHQUFUO1FBQUEsaUJBZ0RDO1FBL0NHLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLEVBQUU7WUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLENBQUM7U0FDeEY7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1NBQ3RGO1FBRUQseUJBQXlCO1FBQ3pCLHNDQUFzQztRQUN0QywwQkFBMEI7UUFDMUIsV0FBVztRQUNYLDJCQUEyQjtRQUMzQixJQUFJO1FBRUosSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLDJDQUEyQztZQUMzQyxJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtnQkFDaEMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0gsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDeEQ7UUFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFUix3QkFBd0I7UUFDeEIsMEVBQTBFO1FBQzFFLElBQUk7UUFFSixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNwQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUVsQyxJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtnQkFDaEMsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7YUFDdEY7aUJBQ0k7Z0JBQ0QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7YUFDcEY7WUFDRCw0QkFBNEI7WUFDNUIsOEJBQThCO1FBQ2xDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNqRCxJQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQztZQUNiLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsOENBQWdCLEdBQWhCLFVBQWlCLEtBQWdCO1FBQzdCLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsR0FBRyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDckMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDdkUsR0FBRyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4RCxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNkLENBQUM7SUEzSkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytEQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQVUsQ0FBQyxFQUFFLENBQUM7MkRBQ0M7SUFHekM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDRDtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3VEQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7eURBQ0E7SUFHckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzREQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MERBQ0M7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzsyREFDRTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzREQUNHO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7eURBQ0E7SUFHckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDSDtJQUdsQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3NEQUNIO0lBMUNELG1CQUFtQjtRQUR2QyxPQUFPO09BQ2EsbUJBQW1CLENBK0p2QztJQUFELDBCQUFDO0NBL0pELEFBK0pDLENBL0pnRCxFQUFFLENBQUMsU0FBUyxHQStKNUQ7a0JBL0pvQixtQkFBbUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3Rpb25UeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhcmFjdGVyQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBKb3lzdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShBY3Rpb25UeXBlKSB9KVxyXG4gICAgYWN0aW9uVHlwZTogQWN0aW9uVHlwZSA9IEFjdGlvblR5cGUuSURMRTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkOiBudW1iZXIgPSAyNTA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBXZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltZUFuaW06IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZUJ1YTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcclxuICAgIGVmZmVjdFNtb2tlOiBjYy5QcmVmYWIgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBMZWZ0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBSaWdodDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wQm90dG9tOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBsZXZlbDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNjYWxlOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIHJpZ2lkYm9keTogY2MuUmlnaWRCb2R5M0Q7XHJcbiAgICBjb2xsaWRlcjogY2MuQ29sbGlkZXIzRDtcclxuXHJcbiAgICBvcmlnaW5hbFNwZWVkOiBudW1iZXIgPSAwO1xyXG4gICAgYXR0YWNrOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZSA9IG51bGw7XHJcbiAgICBib29sUGxheVNvdW5kRm9vdDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8vIG9uTG9hZCAoKSB7fVxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UgPSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpO1xyXG4gICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IGNjLnYyKDAsIDApO1xyXG4gICAgICAgIHRoaXMub3JpZ2luYWxTcGVlZCA9IHRoaXMuc3BlZWQ7XHJcbiAgICAgICAgdGhpcy5yaWdpZGJvZHkgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKTtcclxuICAgICAgICBsZXQgcGh5c2ljX21hbWFuZ2VyID0gY2MuZGlyZWN0b3IuZ2V0UGh5c2ljczNETWFuYWdlcigpO1xyXG4gICAgICAgIHRoaXMucmlnaWRib2R5LnNldExpbmVhclZlbG9jaXR5KG5ldyBjYy5WZWMzKDAsMCwgMCkpO1xyXG4gICAgICAgIHBoeXNpY19tYW1hbmdlci5lbmFibGVkID0gdHJ1ZVxyXG4gICAgICAgIHBoeXNpY19tYW1hbmdlci5ncmF2aXR5ID0gY2MudjMoMCwgMCwgLTMwMDApO1xyXG4gICAgICAgIHRoaXMuY29sbGlkZXIgPSB0aGlzLmdldENvbXBvbmVudChjYy5Db2xsaWRlcjNEKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCB0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueSwgdGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIGxldCBQb3NGb3JYID0gdGhpcy5ub2RlLmdldFBvc2l0aW9uKCk7XHJcbiAgICAgICAgICAgIGxldCBQb3NGb3JZID0gdGhpcy5ub2RlLmdldFBvc2l0aW9uKCk7XHJcbiAgICAgICAgICAgIFBvc0ZvclguYWRkU2VsZihHbG9iYWwudG91Y2hQb3MubXVsKHRoaXMuc3BlZWQgLyAxMDApKTtcclxuICAgICAgICAgICAgUG9zRm9yWS5hZGRTZWxmKEdsb2JhbC50b3VjaFBvcy5tdWwodGhpcy5zcGVlZCAvIDEwMCkpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IFBvc0ZvclgueDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBQb3NGb3JZLnk7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5ib29sUGxheVNvdW5kRm9vdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sUGxheVNvdW5kRm9vdCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEZvb3RTdGVwLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sUGxheVNvdW5kRm9vdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSwgMC4zKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgciA9IE1hdGguYXRhbjIoR2xvYmFsLnRvdWNoUG9zLnksIEdsb2JhbC50b3VjaFBvcy54KTtcclxuICAgICAgICAgICAgdmFyIGRlZ3JlZSA9IHIgKiAxODAgLyAoTWF0aC5QSSk7XHJcbiAgICAgICAgICAgIGRlZ3JlZSA9IDM2MCAtIGRlZ3JlZSArIDkwO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuaXMzRE5vZGUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMygtOTAsIDE4MCwgZGVncmVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQXR0YWNraW5nKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAvLyB0aGlzLkhhbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLldlYXBvbi5hY3RpdmUgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5ub2RlLm5hbWUgPT0gXCJNeURlYWRwb29sXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcImNoYXJhY3Rlcl9ib25lc3xhc3Nhc3Npbl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZighR2xvYmFsLnRlbGVwb3J0KSB7XHJcbiAgICAgICAgLy8gICAgIGNjLmxvZygnPT09PT09PScgLCB0aGlzLmF0dGFjaylcclxuICAgICAgICAvLyAgICAgdGhpcy5hdHRhY2sgPSB0cnVlO1xyXG4gICAgICAgIC8vIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMuYXR0YWNrID0gZmFsc2U7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnNwYXduRWZmZWN0U21va2UodGhpcy5lZmZlY3RTbW9rZSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15RGVhZHBvb2xcIikge1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwua2F0YW5hQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEF0dGFjaywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMC41KTtcclxuXHJcbiAgICAgICAgLy8gaWYoR2xvYmFsLnRlbGVwb3J0KSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ19VU19pZGxlXCIpO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LmFjdGl2ZUd1aWRlKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKHRoaXMubm9kZS5uYW1lID09IFwiTXlEZWFkcG9vbFwiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGFzc2Fzc2luX2lkbGVcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHRoaXMuSGFuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gdGhpcy5XZWFwb24uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICB9XHJcblxyXG4gICAgTGV2ZWxVcFBsYXllcigpIHtcclxuICAgICAgICB0aGlzLm5vZGUuc2NhbGVYID0gdGhpcy5ub2RlLnNjYWxlWCArIHRoaXMuc2NhbGU7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNjYWxlWSA9IHRoaXMubm9kZS5zY2FsZVkgKyB0aGlzLnNjYWxlO1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZVogPSB0aGlzLm5vZGUuc2NhbGVaICsgdGhpcy5zY2FsZTtcclxuICAgICAgICBpZih0aGlzLmxldmVsIDwgNClcclxuICAgICAgICAgICAgdGhpcy5sZXZlbCsrO1xyXG4gICAgfVxyXG5cclxuICAgIHNwYXduRWZmZWN0U21va2Uoc21va2U6IGNjLlByZWZhYikge1xyXG4gICAgICAgIGxldCBzbWsgPSBjYy5pbnN0YW50aWF0ZShzbW9rZSk7XHJcbiAgICAgICAgc21rLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIGxldCBwb3MgPSB0aGlzLm5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKHRoaXMucGxhY2VCdWEuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgcG9zID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIocG9zKTtcclxuICAgICAgICBzbWsueCA9IHBvcy54O1xyXG4gICAgICAgIHNtay55ID0gcG9zLnk7XHJcbiAgICAgICAgc21rLnogPSAwO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Enemy/EnemyController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6fb44ar/vdHCqZo6WpsF2GK', 'EnemyController');
// Scripts/Enemy/EnemyController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Random_1 = require("../Common/Random");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnemyController = /** @class */ (function (_super) {
    __extends(EnemyController, _super);
    function EnemyController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampTop = 0;
        _this.clampBottom = 0;
        _this.PointShoot = null;
        _this.bodyDeath = null;
        _this.effectBlood = null;
        _this.posBlood = null;
        _this.moveX = 0;
        _this.moveY = 0;
        _this.moveZ = 0;
        _this.degree = 0;
        _this.boolCheckAttacking = false;
        _this.boolCheckAttacked = false;
        _this.boolEnemyDeath = false;
        _this.checkFollowPlayer = false;
        _this.checkDeath = false;
        return _this;
    }
    EnemyController.prototype.onLoad = function () {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    };
    EnemyController.prototype.update = function () {
        var _this = this;
        if (!this.checkDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
                var maxDistance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolEnemyDeath) {
                            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.getComponent(CharacterController_1.default).LevelUpPlayer();
                            // this.DestroyByKatana();
                            this.scheduleOnce(function () {
                                _this.DestroyByHammer();
                            }, 0.1);
                            this.checkDeath = true;
                            Global_1.default.boolCheckAttacked = false;
                            Global_1.default.boolCheckAttacking = false;
                        }
                    }
                }
            }
        }
    };
    EnemyController.prototype.StartMove = function () {
        var _this = this;
        this.schedule(function () {
            _this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    };
    EnemyController.prototype.DestroyByHammer = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        // this.PointShoot.active = false;
        this.node.eulerAngles = new cc.Vec3(0, 0, this.node.eulerAngles.z);
        // this.node.scaleZ = 100;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 5);
    };
    EnemyController.prototype.DestroyByKatana = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
        this.node.stopAllActions();
        // this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
    };
    EnemyController.prototype.SpawnerBlood = function () {
        this.bodyDeath.active = true;
        var pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 2);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    };
    EnemyController.prototype.SpawnerEffectBlood = function (x, y, z) {
        var EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    };
    EnemyController.prototype.SpawnerBodyDeath = function (timing) {
        var _this = this;
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(function () {
            _this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            _this.node.destroy();
        }, timing);
    };
    EnemyController.prototype.moveEnemy = function () {
        var _this = this;
        if (this.node.name == 'EnemyHunter1') {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hunter_run");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
        }
        if (this.checkFollowPlayer) {
            var Distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
            var duration = Distance / 21;
            this.moveX = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            while (Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    };
    EnemyController.prototype.betweenDegree = function (dirVec, comVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    EnemyController.prototype.EnemyAttack = function () {
        var _this = this;
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(function () {
            _this.boolCheckAttacking = true;
            _this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(function () {
        }, 0.6);
        this.scheduleOnce(function () {
            Global_1.default.boolStartAttacking = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    };
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "PointShoot", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "bodyDeath", void 0);
    __decorate([
        property(cc.Prefab)
    ], EnemyController.prototype, "effectBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "posBlood", void 0);
    EnemyController = __decorate([
        ccclass
    ], EnemyController);
    return EnemyController;
}(cc.Component));
exports.default = EnemyController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcRW5lbXlcXEVuZW15Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFDbkUsK0RBQXdFO0FBQ3hFLDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRTVDO0lBQTZDLG1DQUFZO0lBRHpEO1FBQUEscUVBK01DO1FBM01HLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFHdEIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFHdkIsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUdyQixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsWUFBTSxHQUFXLENBQUMsQ0FBQztRQUNuQix3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLHVCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNuQyxnQkFBVSxHQUFZLEtBQUssQ0FBQzs7SUE0S2hDLENBQUM7SUExS0csZ0NBQU0sR0FBTjtRQUNJLDJEQUEyRDtJQUMvRCxDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUFBLGlCQTBDQztRQXpDTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1TyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVPLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNU8sSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekwsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxjQUFjLEdBQUcsY0FBYyxFQUFFO29CQUNqQyxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtxQkFDSTtvQkFDRCxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbE8sSUFBSSxXQUFXLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsaUJBQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzTixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxFQUFFO29CQUM3QixJQUFJLFFBQVEsR0FBRyxXQUFXLEVBQUU7d0JBQ3hCLElBQUksZ0JBQU0sQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQ2xELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsYUFBYSxFQUFFLENBQUM7NEJBQ25ILDBCQUEwQjs0QkFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQ0FDZCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7NEJBQzNCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzs0QkFDUixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzs0QkFDdkIsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7NEJBQ2pDLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO3lCQUNyQztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7SUFDVCxDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNWLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEQsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25FLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4RCxDQUFDO0lBRUQsc0NBQVksR0FBWjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN2RSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDOUMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkQsV0FBVyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDN0MsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEIsVUFBaUIsTUFBYztRQUEvQixpQkFTQztRQVJHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDZixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQStCQztRQTlCRyxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLGNBQWMsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQztTQUNuRjthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7U0FDckY7UUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsTyxJQUFJLFFBQVEsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLEtBQUssR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEcsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbEcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDbkM7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEYsT0FBTyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUM3RyxJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNyRjtZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0RyxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUNyRixDQUFDLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELHVDQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQW1CQztRQWxCRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3ZFO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1IsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2xDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2RSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDO0lBek1EO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0M7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzt1REFDRTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3FEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7d0RBQ0c7SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt1REFDUztJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NEQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0RBQ1U7SUFHOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDTztJQXhCUixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBOE1uQztJQUFELHNCQUFDO0NBOU1ELEFBOE1DLENBOU00QyxFQUFFLENBQUMsU0FBUyxHQThNeEQ7a0JBOU1vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFJhbmRvbSBmcm9tIFwiLi4vQ29tbW9uL1JhbmRvbVwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVuZW15Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcExlZnQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFJpZ2h0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcEJvdHRvbTogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIFBvaW50U2hvb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYm9keURlYXRoOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgZWZmZWN0Qmxvb2Q6IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwb3NCbG9vZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgbW92ZVg6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWTogbnVtYmVyID0gMDtcclxuICAgIG1vdmVaOiBudW1iZXIgPSAwO1xyXG4gICAgZGVncmVlOiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbEVuZW15RGVhdGg6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGNoZWNrRm9sbG93UGxheWVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8vIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuQm94Q29sbGlkZXIzRCkuZW5hYmxlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgJiYgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBwb3MxID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jaGlsZHJlblswXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcG9zMyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY2hpbGRyZW5bMl0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24xID0gY2MudjIocG9zMS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MxLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24yID0gY2MudjIocG9zMi54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MyLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWdyZWUgPSBkaXJlY3Rpb24xLnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgICAgICBkZWdyZWUgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcG9zRW5lbXkgPSBjYy52Mih0aGlzLm5vZGUueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgdGhpcy5ub2RlLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMSA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24xKTtcclxuICAgICAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MyID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVhbE5lZWQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZWdyZWVXaXRoUG9zMSA+IGRlZ3JlZVdpdGhQb3MyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBsZXQgZGlzdGFuY2UgPSBVdGlsaXR5Lkluc3RhbmNlKFV0aWxpdHkpLkRpc3RhbmNlKGNjLnYyKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSksIGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSksIGNjLnYyKHBvczMueCwgcG9zMy55KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKE1hdGguYWJzKHJlYWxOZWVkKSA8IGRlZ3JlZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGlzdGFuY2UgPCBtYXhEaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tlZCAmJiAhdGhpcy5ib29sRW5lbXlEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkxldmVsVXBQbGF5ZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLkRlc3Ryb3lCeUthdGFuYSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5EZXN0cm95QnlIYW1tZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAwLjEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3RhcnRNb3ZlKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVFbmVteSgpO1xyXG4gICAgICAgIH0sIDYuNSwgY2MubWFjcm8uUkVQRUFUX0ZPUkVWRVIsIDAuMSk7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5SGFtbWVyKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICAvLyB0aGlzLlBvaW50U2hvb3QuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMCwgMCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIC8vIHRoaXMubm9kZS5zY2FsZVogPSAxMDA7XHJcbiAgICAgICAgdGhpcy5ub2RlLnogPSB0aGlzLm1vdmVaICsgMTtcclxuICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJsb29kKCk7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyRWZmZWN0Qmxvb2QodGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueiArIDUpO1xyXG4gICAgfVxyXG5cclxuICAgIERlc3Ryb3lCeUthdGFuYSgpIHtcclxuICAgICAgICB0aGlzLmJvb2xFbmVteURlYXRoID0gdHJ1ZTtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZFNjcmVhbSwgZmFsc2UpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQucGx1c0VuZW15KTtcclxuICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LnNjYWxlKTtcclxuICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJvZHlEZWF0aCgwLjcpO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckVmZmVjdEJsb29kKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIC8vIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgIH1cclxuXHJcbiAgICBTcGF3bmVyQmxvb2QoKSB7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBvc0Jsb29kLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguc2V0UG9zaXRpb24ocG9zLngsIHBvcy55LCAyKTtcclxuICAgICAgICB0aGlzLmJvZHlEZWF0aC5ldWxlckFuZ2xlcyA9IGNjLnYzKDkwLCAwLCB0aGlzLm5vZGUuZXVsZXJBbmdsZXMueik7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckVmZmVjdEJsb29kKHg6IG51bWJlciwgeTogbnVtYmVyLCB6OiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgRWZmZWN0Qmxvb2QgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmVmZmVjdEJsb29kKTtcclxuICAgICAgICBFZmZlY3RCbG9vZC5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBFZmZlY3RCbG9vZC54ID0geDtcclxuICAgICAgICBFZmZlY3RCbG9vZC55ID0geTtcclxuICAgICAgICBFZmZlY3RCbG9vZC56ID0geiArIDE7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckJvZHlEZWF0aCh0aW1pbmc6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguc2V0UG9zaXRpb24odGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueik7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguZXVsZXJBbmdsZXMgPSBjYy52MygtOTAsIDE4MCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYm9keURlYXRoLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikuc3RvcCgpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgIH0sIHRpbWluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgbW92ZUVuZW15KCkge1xyXG4gICAgICAgIGlmKHRoaXMubm9kZS5uYW1lID09ICdFbmVteUh1bnRlcjEnKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJjaGFyYWN0ZXJfYm9uZXN8aHVudGVyX3J1blwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9ydW5fMlwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY2hlY2tGb2xsb3dQbGF5ZXIpIHtcclxuICAgICAgICAgICAgbGV0IERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSkpO1xyXG4gICAgICAgICAgICBsZXQgZHVyYXRpb24gPSBEaXN0YW5jZSAvIDIxO1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54O1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVZID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55O1xyXG4gICAgICAgICAgICB0aGlzLmRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgLSA5MDtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oZHVyYXRpb24sIHsgcG9zaXRpb246IGNjLnYzKHRoaXMubW92ZVgsIHRoaXMubW92ZVksIHRoaXMubW92ZVopIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FbmVteUF0dGFjaygpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVggPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWSA9IFJhbmRvbS5JbnN0YW5jZShSYW5kb20pLlJhbmRvbVJhbmdlKHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICB3aGlsZSAoVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgPCAxMDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubW92ZVggPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubW92ZVkgPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wQm90dG9tLCB0aGlzLmNsYW1wVG9wKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgKyA5MDtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oNiwgeyBwb3NpdGlvbjogY2MudjModGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSwgdGhpcy5tb3ZlWikgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5yb3RhdGUzRFRvKDAuMiwgY2MudjMoLTkwLCAtMTgwLCAtdGhpcy5kZWdyZWUpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYmV0d2VlbkRlZ3JlZShkaXJWZWMsIGNvbVZlYykge1xyXG4gICAgICAgIGxldCBhbmdsZURlZyA9IE1hdGguYXRhbjIoZGlyVmVjLnkgLSBjb21WZWMueSwgZGlyVmVjLnggLSBjb21WZWMueCkgKiAxODAgLyBNYXRoLlBJO1xyXG4gICAgICAgIHJldHVybiBhbmdsZURlZztcclxuICAgIH1cclxuXHJcbiAgICBFbmVteUF0dGFjaygpIHtcclxuICAgICAgICB0aGlzLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15VmVub21cIikge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQW1vbmdVc19BdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSGFtbWVyIEF0dGFja1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0F0dGFja2luZyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEF0dGFjaywgZmFsc2UpO1xyXG4gICAgICAgIH0sIDAuNSk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgIH0sIDAuNik7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ19VU19pZGxlXCIpO1xyXG4gICAgICAgIH0sIDEpO1xyXG4gICAgfVxyXG5cclxufSJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnumDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf993Azr2BF7p40pHHz6qKS', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionType;
(function (ActionType) {
    ActionType[ActionType["IDLE"] = 0] = "IDLE";
    ActionType[ActionType["RUN"] = 1] = "RUN";
    ActionType[ActionType["ATTACK"] = 2] = "ATTACK";
    ActionType[ActionType["VICTORY"] = 3] = "VICTORY";
    ActionType[ActionType["SCARE"] = 4] = "SCARE";
    ActionType[ActionType["NORMAL"] = 5] = "NORMAL";
    ActionType[ActionType["FAST"] = 6] = "FAST";
})(ActionType = exports.ActionType || (exports.ActionType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbnVtRGVmaW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBWSxVQVNYO0FBVEQsV0FBWSxVQUFVO0lBRWxCLDJDQUFJLENBQUE7SUFDSix5Q0FBRyxDQUFBO0lBQ0gsK0NBQU0sQ0FBQTtJQUNOLGlEQUFPLENBQUE7SUFDUCw2Q0FBSyxDQUFBO0lBQ0wsK0NBQU0sQ0FBQTtJQUNOLDJDQUFJLENBQUE7QUFDUixDQUFDLEVBVFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFTckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBBY3Rpb25UeXBlXHJcbntcclxuICAgIElETEUsXHJcbiAgICBSVU4sXHJcbiAgICBBVFRBQ0ssXHJcbiAgICBWSUNUT1JZLFxyXG4gICAgU0NBUkUsXHJcbiAgICBOT1JNQUwsXHJcbiAgICBGQVNULFxyXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Camera/CameraFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a3c4BNdsNGOKf91Tc0+3pS', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.gameplayInstance = null;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        _this.Target = null;
        //9,12
        _this.plusY = 0;
        _this.plusZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 43;
        this.cameraOffsetZ = this.node.z;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay && !Global_1.default.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if (Global_1.default.boolendG) {
            cc.log('true');
            this.node.x = 0;
            this.node.y = -124;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38, 0, 0);
        }
        // else if (!Global.teleport) {
        //     Global.boolCheckTele = true;
        //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX + 20, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY + 20, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ - 50, 0.2);
        //     // this.node.x = -87;
        //     // this.node.y = 60;
        //     // this.node.z = 76.5;
        //     this.node.eulerAngles = new cc.Vec3(45.5, -2, 22);
        // }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    };
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    CameraFollow.prototype.PlusYZ = function () {
        this.plusY += 12;
        this.plusZ += 9;
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2FtZXJhXFxDYW1lcmFGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUR0RDtRQUFBLHFFQTBHQztRQXhHRyxzQkFBZ0IsR0FBcUIsSUFBSSxDQUFDO1FBQzFDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsTUFBTTtRQUNOLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQzs7SUFpR3RCLENBQUM7SUFoR0csNEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFDRCw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsUUFBUSxFQUFFO1lBQzFDLGtGQUFrRjtZQUNsRixrRkFBa0Y7WUFDbEYseUJBQXlCO1lBQ3pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDekYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNuSDtZQUNELHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osc0dBQXNHO1lBQ3RHLHVIQUF1SDtZQUN2SCxvSUFBb0k7WUFDcEksb0lBQW9JO1lBQ3BJLElBQUk7WUFDSixzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osR0FBRztpQkFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLDZCQUE2QjtnQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3ZIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO1NBQ0o7YUFDSSxJQUFHLGdCQUFNLENBQUMsUUFBUSxFQUFDO1lBQ3BCLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsK0JBQStCO1FBQy9CLG1DQUFtQztRQUNuQyw0SEFBNEg7UUFDNUgsNEhBQTRIO1FBQzVILDRIQUE0SDtRQUM1SCw0QkFBNEI7UUFDNUIsMkJBQTJCO1FBQzNCLDZCQUE2QjtRQUM3Qix5REFBeUQ7UUFDekQsSUFBSTtRQUNKLFNBQVM7UUFDVCwyRkFBMkY7UUFDM0YsZ0ZBQWdGO1FBQ2hGLFFBQVE7UUFDUiwwQkFBMEI7UUFDMUIsdUVBQXVFO1FBQ3ZFLHdGQUF3RjtRQUN4Rix3RkFBd0Y7UUFDeEYsR0FBRztRQUNILEdBQUc7SUFDUCxDQUFDO0lBQ0QsaURBQWlEO0lBQ2pELG1EQUFtRDtJQUNuRCxtREFBbUQ7SUFDbkQsbURBQW1EO0lBQ25ELElBQUk7SUFDSiw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQXhHZ0IsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQXlHaEM7SUFBRCxtQkFBQztDQXpHRCxBQXlHQyxDQXpHeUMsRUFBRSxDQUFDLFNBQVMsR0F5R3JEO2tCQXpHb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FtZXJhRm9sbG93IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIGdhbWVwbGF5SW5zdGFuY2U6IEdhbWVQbGF5SW5zdGFuY2UgPSBudWxsO1xyXG4gICAgY2FtZXJhT2Zmc2V0WDogbnVtYmVyID0gMDtcclxuICAgIGNhbWVyYU9mZnNldFk6IG51bWJlciA9IDA7XHJcbiAgICBjYW1lcmFPZmZzZXRaOiBudW1iZXIgPSAwO1xyXG4gICAgVGFyZ2V0OiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIC8vOSwxMlxyXG4gICAgcGx1c1k6IG51bWJlciA9IDA7XHJcbiAgICBwbHVzWjogbnVtYmVyID0gMDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSk7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRYID0gdGhpcy5ub2RlLnggLSAwO1xyXG4gICAgICAgIHRoaXMuY2FtZXJhT2Zmc2V0WSA9IHRoaXMubm9kZS55ICsgNDM7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRaID0gdGhpcy5ub2RlLno7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgLy9sZXQgbmV3UG9zWCA9IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYO1xyXG4gICAgICAgICAgICAvL2xldCBuZXdQb3NaID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFo7XHJcbiAgICAgICAgICAgIC8vaWYgKCFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaLCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy8gZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAyKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSB0aGlzLnBsdXNZLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgdGhpcy5wbHVzWiwgMC4yKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAvLyBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDMpIHtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHRoaXMucGx1c1ksIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyB0aGlzLnBsdXNaLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy99XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnJlc2V0T2Zmc2V0KDAsIDEyLCA5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDEyLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgOSwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDI0LCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgMTgsIDAuMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSAzNiwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIDI3LCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gNDgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyAzNiwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKEdsb2JhbC5ib29sZW5kRyl7XHJcbiAgICAgICAgICAgIGNjLmxvZygndHJ1ZScpXHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSAtMTI0O1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueiA9IDEyMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMzgsMCwwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZWxzZSBpZiAoIUdsb2JhbC50ZWxlcG9ydCkge1xyXG4gICAgICAgIC8vICAgICBHbG9iYWwuYm9vbENoZWNrVGVsZSA9IHRydWU7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCArIDIwLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgKyAyMCwgMC4yKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaIC0gNTAsIDAuMik7XHJcbiAgICAgICAgLy8gICAgIC8vIHRoaXMubm9kZS54ID0gLTg3O1xyXG4gICAgICAgIC8vICAgICAvLyB0aGlzLm5vZGUueSA9IDYwO1xyXG4gICAgICAgIC8vICAgICAvLyB0aGlzLm5vZGUueiA9IDc2LjU7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKDQ1LjUsIC0yLCAyMik7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vIGVsc2Uge1xyXG4gICAgICAgIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuVGFyZ2V0ID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5LmVuZW15UGFyZW50LmNoaWxkcmVuW2ldO1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIHRoaXMucmVzZXRPZmZzZXQoKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuVGFyZ2V0LnggLyAyLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5UYXJnZXQueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSwgMC4yKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuVGFyZ2V0LnogKyB0aGlzLmNhbWVyYU9mZnNldFosIDAuMik7XHJcbiAgICAgICAgLy99XHJcbiAgICAgICAgLy99XHJcbiAgICB9XHJcbiAgICAvLyByZXNldE9mZnNldCh4OiBudW1iZXIsIHk6IG51bWJlciwgejogbnVtYmVyKSB7XHJcbiAgICAvLyAgICAgdGhpcy5jYW1lcmFPZmZzZXRYID0gdGhpcy5jYW1lcmFPZmZzZXRYIC0geDtcclxuICAgIC8vICAgICB0aGlzLmNhbWVyYU9mZnNldFkgPSB0aGlzLmNhbWVyYU9mZnNldFkgLSB5O1xyXG4gICAgLy8gICAgIHRoaXMuY2FtZXJhT2Zmc2V0WiA9IHRoaXMuY2FtZXJhT2Zmc2V0WiArIHo7XHJcbiAgICAvLyB9XHJcbiAgICBQbHVzWVooKSB7XHJcbiAgICAgICAgdGhpcy5wbHVzWSArPSAxMjtcclxuICAgICAgICB0aGlzLnBsdXNaICs9IDk7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3079c9yA9NNG7yWTNHi7OqT', 'GamePlayInstance');
// Scripts/Common/GamePlayInstance.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// import Smasher1 from "../GamePlay/Smasher1,2/Smasher1";
// import Smasher5 from "../GamePlay/Smasher5/Smasher5";
var MineCraft1_1 = require("../GamePlay/MC1/MineCraft1");
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
exports.instance = new cc.EventTarget();
var GamePlayInstance = /** @class */ (function (_super) {
    __extends(GamePlayInstance, _super);
    function GamePlayInstance() {
        var _this = _super.call(this) || this;
        _this.gameplay = MineCraft1_1.default.Instance(MineCraft1_1.default);
        GamePlayInstance_1._instance = _this;
        return _this;
    }
    GamePlayInstance_1 = GamePlayInstance;
    var GamePlayInstance_1;
    GamePlayInstance = GamePlayInstance_1 = __decorate([
        ccclass
    ], GamePlayInstance);
    return GamePlayInstance;
}(Singleton_1.default));
exports.default = GamePlayInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDBEQUEwRDtBQUMxRCx3REFBd0Q7QUFDeEQseURBQW9EO0FBRXBELHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBQy9CLFFBQUEsUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBRTdDO0lBQThDLG9DQUEyQjtJQUVyRTtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQUpELGNBQVEsR0FBZSxvQkFBVSxDQUFDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDLENBQUM7UUFHbkQsa0JBQWdCLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDdEMsQ0FBQzt5QkFMZ0IsZ0JBQWdCOztJQUFoQixnQkFBZ0I7UUFEcEMsT0FBTztPQUNhLGdCQUFnQixDQU1wQztJQUFELHVCQUFDO0NBTkQsQUFNQyxDQU42QyxtQkFBUyxHQU10RDtrQkFOb0IsZ0JBQWdCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW1wb3J0IFNtYXNoZXIxIGZyb20gXCIuLi9HYW1lUGxheS9TbWFzaGVyMSwyL1NtYXNoZXIxXCI7XHJcbi8vIGltcG9ydCBTbWFzaGVyNSBmcm9tIFwiLi4vR2FtZVBsYXkvU21hc2hlcjUvU21hc2hlcjVcIjtcclxuaW1wb3J0IE1pbmVDcmFmdDEgZnJvbSBcIi4uL0dhbWVQbGF5L01DMS9NaW5lQ3JhZnQxXCI7XHJcbmltcG9ydCBTbWFzaGVyMSBmcm9tIFwiLi4vR2FtZVBsYXkvU1MxL1NtYXNoZXIxXCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4vU2luZ2xldG9uXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5leHBvcnQgY29uc3QgaW5zdGFuY2UgPSBuZXcgY2MuRXZlbnRUYXJnZXQoKTtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZVBsYXlJbnN0YW5jZSBleHRlbmRzIFNpbmdsZXRvbjxHYW1lUGxheUluc3RhbmNlPiB7XHJcbiAgICBnYW1lcGxheTogTWluZUNyYWZ0MSA9IE1pbmVDcmFmdDEuSW5zdGFuY2UoTWluZUNyYWZ0MSk7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Vent/VentCtrl.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '47feeRjYpxKSKMq10SbyDma', 'VentCtrl');
// Scripts/Vent/VentCtrl.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var VentCtrl = /** @class */ (function (_super) {
    __extends(VentCtrl, _super);
    function VentCtrl() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.xMin = 0;
        _this.xMax = 0;
        _this.yMin = 0;
        _this.yMax = 0;
        _this.vent2 = null;
        _this.boolCheckTele = false;
        return _this;
    }
    VentCtrl.prototype.onLoad = function () { };
    VentCtrl.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
    };
    VentCtrl.prototype.update = function (dt) {
        var _this = this;
        var character = this.gameplayInstance.gameplay.MyCharacter;
        if (this.xMin < character.x && this.xMax > character.x && this.yMin < character.y && this.yMax > character.y && !Global_1.default.boolCheckTele) {
            Global_1.default.boolCheckTele = true;
            Global_1.default.enableAttack = false;
            Global_1.default.boolEnableTouch = false;
            character.getComponent(CharacterController_1.default).enabled = false;
            character.getComponent(cc.SkeletonAnimation).play('Among_US_idle');
            this.node.getComponent(cc.Animation).play('Vent');
            character.getComponent(CharacterController_1.default).JoystickFollow.active = false;
            character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
            var tween = new cc.Tween().to(0.3, { position: cc.v3(this.node.x + 2, this.node.y, this.node.z + 10) }).to(0.65, { scale: 0 });
            tween.target(character).start();
            this.scheduleOnce(function () {
                Global_1.default.boolendG = true;
                Global_1.default.boolStartAttacking = false;
                var posVent2 = _this.vent2.getPosition();
                _this.vent2.getComponent(cc.Animation).play('Vent');
                var tween = new cc.Tween().to(0.5, { position: cc.v3(posVent2.x, posVent2.y, 20) }, { easing: 'easeBackIn' }).to(0.5, { scale: 500 }).call(function () {
                });
                tween.target(character).start();
                character.setPosition(cc.v3(posVent2.x + 10, posVent2.y - 40, -15));
                character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
                character.z = 55;
                character.scale = 500;
                character.angle = 80;
                character.eulerAngles = new cc.Vec3(-90, 180, -24);
            }, 0.8);
        }
    };
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMax", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMax", void 0);
    __decorate([
        property(cc.Node)
    ], VentCtrl.prototype, "vent2", void 0);
    VentCtrl = __decorate([
        ccclass
    ], VentCtrl);
    return VentCtrl;
}(cc.Component));
exports.default = VentCtrl;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVmVudFxcVmVudEN0cmwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUFzQyw0QkFBWTtJQURsRDtRQUFBLHFFQXlEQztRQXJERyxVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBR2pCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFHakIsVUFBSSxHQUFXLENBQUMsQ0FBQztRQUdqQixVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBR2pCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsbUJBQWEsR0FBWSxLQUFLLENBQUM7O0lBc0NuQyxDQUFDO0lBcENHLHlCQUFNLEdBQU4sY0FBVyxDQUFDO0lBRVosd0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFPLEVBQUU7UUFBVCxpQkE2QkM7UUE1QkcsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFDM0QsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLGFBQWEsRUFBRTtZQUNuSSxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDNUIsZ0JBQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzVCLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUMvQixTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUM1RCxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2xELFNBQVMsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUMxRSxTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDMUUsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdILEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN4QyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNuRCxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQyxNQUFNLEVBQUUsWUFBWSxFQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN6SSxDQUFDLENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNoQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNwRSxTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQzFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNqQixTQUFTLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztnQkFDdEIsU0FBUyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtTQUNWO0lBQ0wsQ0FBQztJQXBERDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBDQUNKO0lBR2pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MENBQ0o7SUFHakI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzswQ0FDSjtJQUdqQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBDQUNKO0lBR2pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0k7SUFmTCxRQUFRO1FBRDVCLE9BQU87T0FDYSxRQUFRLENBd0Q1QjtJQUFELGVBQUM7Q0F4REQsQUF3REMsQ0F4RHFDLEVBQUUsQ0FBQyxTQUFTLEdBd0RqRDtrQkF4RG9CLFFBQVEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWZW50Q3RybCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICB4TWluOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgeE1heDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHlNaW46IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICB5TWF4OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdmVudDI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGdhbWVwbGF5SW5zdGFuY2U6IEdhbWVQbGF5SW5zdGFuY2U7XHJcbiAgICBib29sQ2hlY2tUZWxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgb25Mb2FkKCkgeyB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoZHQpIHtcclxuICAgICAgICBsZXQgY2hhcmFjdGVyID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyO1xyXG4gICAgICAgIGlmICh0aGlzLnhNaW4gPCBjaGFyYWN0ZXIueCAmJiB0aGlzLnhNYXggPiBjaGFyYWN0ZXIueCAmJiB0aGlzLnlNaW4gPCBjaGFyYWN0ZXIueSAmJiB0aGlzLnlNYXggPiBjaGFyYWN0ZXIueSAmJiAhR2xvYmFsLmJvb2xDaGVja1RlbGUpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja1RlbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuZW5hYmxlQXR0YWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGNoYXJhY3Rlci5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoJ0Ftb25nX1VTX2lkbGUnKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5BbmltYXRpb24pLnBsYXkoJ1ZlbnQnKTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5Kb3lzdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC4zLCB7cG9zaXRpb246IGNjLnYzKHRoaXMubm9kZS54ICsgMiwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56ICsgMTApfSkudG8oMC42NSwgeyBzY2FsZTogMCB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KGNoYXJhY3Rlcikuc3RhcnQoKTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xlbmRHID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHZhciBwb3NWZW50MiA9IHRoaXMudmVudDIuZ2V0UG9zaXRpb24oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmVudDIuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbikucGxheSgnVmVudCcpO1xyXG4gICAgICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC41LCB7IHBvc2l0aW9uOiBjYy52Myhwb3NWZW50Mi54LCBwb3NWZW50Mi55LCAyMCkgfSwge2Vhc2luZzogJ2Vhc2VCYWNrSW4nfSkudG8oMC41LCB7IHNjYWxlOiA1MDAgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHR3ZWVuLnRhcmdldChjaGFyYWN0ZXIpLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuc2V0UG9zaXRpb24oY2MudjMocG9zVmVudDIueCArIDEwLCBwb3NWZW50Mi55IC0gNDAsIC0xNSkpO1xyXG4gICAgICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNoYXJhY3Rlci56ID0gNTU7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuc2NhbGUgPSA1MDA7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuYW5nbGUgPSA4MDtcclxuICAgICAgICAgICAgICAgIGNoYXJhY3Rlci5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKC05MCwgMTgwLCAtMjQpO1xyXG4gICAgICAgICAgICB9LCAwLjgpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Joystick/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '73034Tph19N/64CSzvxwsiQ', 'JoystickFollow');
// Scripts/Joystick/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoystickFollow = /** @class */ (function (_super) {
    __extends(JoystickFollow, _super);
    function JoystickFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
    }
    JoystickFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoystickFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
    };
    JoystickFollow.prototype.touchStart = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            var mousePos = event.getLocation();
            var localMousePos = this.node.convertToNodeSpaceAR(mousePos);
            this.node.opacity = 255;
            this.stickPos = localMousePos;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePos);
            this.joyDot.setPosition(localMousePos);
            this.gameplayInstance.gameplay.Guide.active = false;
            // this.gameplayInstance.gameplay.txtSmasher.active = false;
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoystickFollow.prototype.touchMove = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                if (this.gameplayInstance.gameplay.MyCharacter.name == 'MyDeadpool') {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_run");
                }
                else {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
                }
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gameplayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
        }
        this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
    };
    JoystickFollow.prototype.touchCancel = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            Global_1.default.boolEnableTouch = false;
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).Attacking();
            this.node.opacity = 0;
            Global_1.default.boolFirstTouchJoyStick = false;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyDot", void 0);
    JoystickFollow = __decorate([
        ccclass
    ], JoystickFollow);
    return JoystickFollow;
}(cc.Component));
exports.default = JoystickFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcSm95c3RpY2tcXEpveXN0aWNrRm9sbG93LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHdFQUFtRTtBQUNuRSwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBRWhDLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBNEMsa0NBQVk7SUFEeEQ7UUFBQSxxRUFxRkM7UUFqRkcsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFDekIsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFDOUIsWUFBTSxHQUFXLENBQUMsQ0FBQzs7SUEwRXZCLENBQUM7SUF2RUcsK0JBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCw4QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxtQ0FBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQUcsQ0FBQyxnQkFBTSxDQUFDLFFBQVEsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDdkUsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25DLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEQsNERBQTREO1lBQzVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQzdHO0lBQ0wsQ0FBQztJQUVELGtDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ1gsSUFBRyxDQUFDLGdCQUFNLENBQUMsUUFBUSxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUN2RSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7WUFDeEIsZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUcsQ0FBQyxnQkFBTSxDQUFDLHNCQUFzQixFQUFFO2dCQUMvQixnQkFBTSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztnQkFDckMsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksWUFBWSxFQUFFO29CQUNoRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7aUJBQ3RIO3FCQUFNO29CQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQztpQkFDdEg7YUFFSjtZQUNELElBQUcsSUFBSSxDQUFDLGFBQWEsS0FBSyxLQUFLLENBQUMsV0FBVyxFQUFFLEVBQUM7Z0JBQzFDLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM5QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN0RSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBRyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUM5QztpQkFBTTtnQkFDSCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztTQUNKO1FBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDOUcsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxLQUFLO1FBQ2IsSUFBSSxDQUFDLGdCQUFNLENBQUMsUUFBUSxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUN4RSxnQkFBTSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3pGLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUN0QixnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztTQUN6QztJQUNMLENBQUM7SUFoRkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNLO0lBTk4sY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQW9GbEM7SUFBRCxxQkFBQztDQXBGRCxBQW9GQyxDQXBGMkMsRUFBRSxDQUFDLFNBQVMsR0FvRnZEO2tCQXBGb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9DaGFyYWN0ZXIvQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5SW5zdGFuY2VcIjtcclxuaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBKb3lzdGlja0ZvbGxvdyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lSaW5nOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveURvdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgc3RpY2tQb3M6IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgdG91Y2hMb2NhdGlvbjogY2MuVmVjMiA9IG51bGw7XHJcbiAgICByYWRpdXM6IG51bWJlciA9IDA7XHJcbiAgICBnYW1lcGxheUluc3RhbmNlOiBHYW1lUGxheUluc3RhbmNlO1xyXG5cclxuICAgIG9uTG9hZCAoKSB7XHJcbiAgICAgICAgdGhpcy5yYWRpdXMgPSB0aGlzLmpveVJpbmcud2lkdGggLyAyO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UgPSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9TVEFSVCwgdGhpcy50b3VjaFN0YXJ0LCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfTU9WRSwgdGhpcy50b3VjaE1vdmUsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9FTkQsIHRoaXMudG91Y2hDYW5jZWwsIHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9DQU5DRUwsIHRoaXMudG91Y2hDYW5jZWwsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvdWNoU3RhcnQoZXZlbnQpIHtcclxuICAgICAgICBpZighR2xvYmFsLmJvb2xlbmRHICYmIEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIHZhciBtb3VzZVBvcyA9IGV2ZW50LmdldExvY2F0aW9uKCk7XHJcbiAgICAgICAgICAgIGxldCBsb2NhbE1vdXNlUG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKG1vdXNlUG9zKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgIHRoaXMuc3RpY2tQb3MgPSBsb2NhbE1vdXNlUG9zO1xyXG4gICAgICAgICAgICB0aGlzLnRvdWNoTG9jYXRpb24gPSBldmVudC5nZXRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgICB0aGlzLmpveVJpbmcuc2V0UG9zaXRpb24obG9jYWxNb3VzZVBvcyk7XHJcbiAgICAgICAgICAgIHRoaXMuam95RG90LnNldFBvc2l0aW9uKGxvY2FsTW91c2VQb3MpO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS50eHRTbWFzaGVyLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRvdWNoTW92ZShldmVudCkge1xyXG4gICAgICAgIGlmKCFHbG9iYWwuYm9vbGVuZEcgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgJiYgIUdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSB0cnVlO1xyXG4gICAgICAgICAgICBpZighR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2spIHtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5uYW1lID09ICdNeURlYWRwb29sJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJjaGFyYWN0ZXJfYm9uZXN8YXNzYXNzaW5fcnVuXCIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9ydW5fMlwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKHRoaXMudG91Y2hMb2NhdGlvbiA9PT0gZXZlbnQuZ2V0TG9jYXRpb24oKSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgbGV0IHRvdWNoUG9zID0gdGhpcy5qb3lSaW5nLmNvbnZlcnRUb05vZGVTcGFjZUFSKGV2ZW50LmdldExvY2F0aW9uKCkpO1xyXG4gICAgICAgICAgICBsZXQgZGlzdGFuY2UgPSB0b3VjaFBvcy5tYWcoKTtcclxuICAgICAgICAgICAgbGV0IHBvc1ggPSB0aGlzLnN0aWNrUG9zLnggKyB0b3VjaFBvcy54O1xyXG4gICAgICAgICAgICBsZXQgcG9zWSA9IHRoaXMuc3RpY2tQb3MueSArIHRvdWNoUG9zLnk7XHJcbiAgICAgICAgICAgIGxldCBwID0gY2MudjIocG9zWCwgcG9zWSkuc3ViKHRoaXMuam95UmluZy5nZXRQb3NpdGlvbigpKS5ub3JtYWxpemUoKTtcclxuICAgICAgICAgICAgR2xvYmFsLnRvdWNoUG9zID0gcDtcclxuICAgICAgICAgICAgaWYodGhpcy5yYWRpdXMgPiBkaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24oY2MudjIocG9zWCwgcG9zWSkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHggPSB0aGlzLnN0aWNrUG9zLnggKyBwLnggKiB0aGlzLnJhZGl1cztcclxuICAgICAgICAgICAgICAgIGxldCB5ID0gdGhpcy5zdGlja1Bvcy55ICsgcC55ICogdGhpcy5yYWRpdXM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihjYy52Mih4LCB5KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHRvdWNoQ2FuY2VsKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbGVuZEcgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgJiYgIUdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmFibGVUb3VjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXR0YWNraW5nKCk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/VFX/DestroyVFX.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd0b6c4IwSBMe46SP3WrqOM5', 'DestroyVFX');
// Scripts/VFX/DestroyVFX.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DestroyVFX = /** @class */ (function (_super) {
    __extends(DestroyVFX, _super);
    function DestroyVFX() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.timming = 0;
        return _this;
    }
    DestroyVFX.prototype.start = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.node.destroy();
        }, this.timming);
    };
    __decorate([
        property(cc.Integer)
    ], DestroyVFX.prototype, "timming", void 0);
    DestroyVFX = __decorate([
        ccclass
    ], DestroyVFX);
    return DestroyVFX;
}(cc.Component));
exports.default = DestroyVFX;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVkZYXFxEZXN0cm95VkZYLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBd0MsOEJBQVk7SUFEcEQ7UUFBQSxxRUFVQztRQU5HLGFBQU8sR0FBVyxDQUFDLENBQUM7O0lBTXhCLENBQUM7SUFMRywwQkFBSyxHQUFMO1FBQUEsaUJBSUM7UUFIRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFMRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOytDQUNEO0lBSEgsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQVM5QjtJQUFELGlCQUFDO0NBVEQsQUFTQyxDQVR1QyxFQUFFLENBQUMsU0FBUyxHQVNuRDtrQkFUb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlc3Ryb3lWRlggZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltbWluZzogbnVtYmVyID0gMDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgICAgICB9LCB0aGlzLnRpbW1pbmcpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/SS2/Smasher2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '08784njWzJFcpWWnr8tSzUD', 'Smasher2');
// Scripts/GamePlay/SS2/Smasher2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var VentCtrl_1 = require("../../Vent/VentCtrl");
var Smasher1_1 = require("../SS1/Smasher1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Smasher2 = /** @class */ (function (_super) {
    __extends(Smasher2, _super);
    function Smasher2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.txtEndGame = null;
        _this.txtStart = null;
        _this.btnAll = null;
        _this.arrow = null;
        _this.vent1 = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.distance = null;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        Smasher1_1.default._instance = _this;
        return _this;
    }
    Smasher2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher2.prototype.start = function () {
        this.playGame();
        this.vent1.enabled = false;
    };
    Smasher2.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtStart.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd) {
            this.vent1.enabled = true;
            this.arrow.active = true;
        }
        if (Global_1.default.boolCheckTele) {
            this.EndGame();
            this.arrow.active = false;
        }
    };
    Smasher2.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.txtEndGame.active = true;
        this.btnAll.active = true;
        this.Guide.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    Smasher2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    Smasher2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    Smasher2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    Smasher2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], Smasher2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "txtEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "txtStart", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher2.prototype, "arrow", void 0);
    __decorate([
        property(VentCtrl_1.default)
    ], Smasher2.prototype, "vent1", void 0);
    Smasher2 = __decorate([
        ccclass
    ], Smasher2);
    return Smasher2;
}(Singleton_1.default));
exports.default = Smasher2;
function getPositionX() {
    throw new Error("Function not implemented.");
}
exports.getPositionX = getPositionX;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFNTMlxcU21hc2hlcjIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0Esa0VBQXlEO0FBQ3pELDhDQUF5QztBQUN6QyxrREFBNkM7QUFDN0Msb0RBQStDO0FBQy9DLCtEQUEwRDtBQUMxRCxnREFBMkM7QUFDM0MsNENBQXVDO0FBRWpDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFLNUM7SUFBc0MsNEJBQW1CO0lBa0RyRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQWxERCxvQkFBYyxHQUFZLElBQUksQ0FBQztRQUcvQixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsV0FBSyxHQUFZLElBQUksQ0FBQztRQUd0QixXQUFLLEdBQWEsSUFBSSxDQUFDO1FBRXZCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsMEJBQW9CLEdBQVksS0FBSyxDQUFDO1FBQ3RDLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUlwQixrQkFBUSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQzlCLENBQUM7SUFFRCwyQkFBUSxHQUFSO1FBQ0ksMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QywyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELDRCQUFTLEdBQVQ7UUFDSSwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9DLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkQsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsd0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVELHlCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUM1QjtRQUVELElBQUcsZ0JBQU0sQ0FBQyxhQUFhLEVBQUU7WUFDckIsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQzdCO0lBQ0wsQ0FBQztJQUVELDBCQUFPLEdBQVA7UUFDSSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCwyQkFBUSxHQUFSO1FBQ0ksZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzFFO0lBQ0wsQ0FBQztJQUVELHdCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNEJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELDhCQUFXLEdBQVg7UUFDSSxJQUFJLGdCQUFNLENBQUMsYUFBYTtZQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQW5JRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7eUNBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzttREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDUztJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NENBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDOzJDQUNJO0lBdkNOLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0F1STVCO0lBQUQsZUFBQztDQXZJRCxBQXVJQyxDQXZJcUMsbUJBQVMsR0F1STlDO2tCQXZJb0IsUUFBUTtBQXlJN0IsU0FBZ0IsWUFBWTtJQUN4QixNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7QUFDakQsQ0FBQztBQUZELG9DQUVDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi8uLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi8uLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFZlbnRDdHJsIGZyb20gXCIuLi8uLi9WZW50L1ZlbnRDdHJsXCI7XHJcbmltcG9ydCBTbWFzaGVyMSBmcm9tIFwiLi4vU1MxL1NtYXNoZXIxXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNtYXNoZXIyIGV4dGVuZHMgU2luZ2xldG9uPFNtYXNoZXIxPiB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lTdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBNYXA6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY291bnRFbmVteUVuZDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE15Q2hhcmFjdGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEVmZmVjdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBlbmVteVBhcmVudDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5Eb3dubG9hZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB0eHRFbmRHYW1lOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkFsbDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBhcnJvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KFZlbnRDdHJsKVxyXG4gICAgdmVudDE6IFZlbnRDdHJsID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBkaXN0YW5jZTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBTbWFzaGVyMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgICAgIHRoaXMudmVudDEuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMudHh0U3RhcnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCkge1xyXG4gICAgICAgICAgICB0aGlzLnZlbnQxLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmFycm93LmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZihHbG9iYWwuYm9vbENoZWNrVGVsZSkge1xyXG4gICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICAgICAgdGhpcy5hcnJvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgRW5kR2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbGVuZEcgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy50eHRFbmRHYW1lLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5idG5BbGwuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5idG5Eb3dubG9hZC5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5pbnRlcmFjdGFibGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbFN0YXJ0UGxheSlcclxuICAgICAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0UG9zaXRpb25YKCkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiRnVuY3Rpb24gbm90IGltcGxlbWVudGVkLlwiKTtcclxufVxyXG5cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/PlatformBtn.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1dd30jCURNKwqdKI3VVurRk', 'PlatformBtn');
// Scripts/Common/PlatformBtn.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PlatformBtn = /** @class */ (function (_super) {
    __extends(PlatformBtn, _super);
    function PlatformBtn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.androidSprites = null;
        _this.iosSprites = null;
        return _this;
    }
    PlatformBtn.prototype.start = function () {
        if (cc.sys.os == cc.sys.OS_ANDROID)
            this.getComponent(cc.Sprite).spriteFrame = this.androidSprites;
        else if (cc.sys.os == cc.sys.OS_IOS)
            this.getComponent(cc.Sprite).spriteFrame = this.iosSprites;
    };
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "androidSprites", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "iosSprites", void 0);
    PlatformBtn = __decorate([
        ccclass
    ], PlatformBtn);
    return PlatformBtn;
}(cc.Component));
exports.default = PlatformBtn;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxQbGF0Zm9ybUJ0bi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBRzFDO0lBQXlDLCtCQUFZO0lBRHJEO1FBQUEscUVBY0M7UUFYRyxvQkFBYyxHQUFtQixJQUFJLENBQUM7UUFFdEMsZ0JBQVUsR0FBbUIsSUFBSSxDQUFDOztJQVN0QyxDQUFDO0lBUkcsMkJBQUssR0FBTDtRQUVJLElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVO1lBQzdCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2FBQzlELElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNO1lBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ25FLENBQUM7SUFURDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3VEQUNhO0lBRXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7bURBQ1M7SUFKakIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWEvQjtJQUFELGtCQUFDO0NBYkQsQUFhQyxDQWJ3QyxFQUFFLENBQUMsU0FBUyxHQWFwRDtrQkFib0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGxhdGZvcm1CdG4gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG4gICAgYW5kcm9pZFNwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuICAgIGlvc1Nwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIHN0YXJ0KClcclxuICAgIHtcclxuICAgICAgICBpZihjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0FORFJPSUQpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmFuZHJvaWRTcHJpdGVzO1xyXG4gICAgICAgIGVsc2UgaWYoY2Muc3lzLm9zID09IGNjLnN5cy5PU19JT1MpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmlvc1Nwcml0ZXM7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/SS1/Smasher1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '743adJDcwFAjoO1TT8zhVhi', 'Smasher1');
// Scripts/GamePlay/SS1/Smasher1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Smasher1 = /** @class */ (function (_super) {
    __extends(Smasher1, _super);
    function Smasher1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.btnContinue = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.txtEndGame = null;
        _this.nStart = null;
        _this.countEnemy = 0;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        Smasher1_1._instance = _this;
        return _this;
    }
    Smasher1_1 = Smasher1;
    Smasher1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Smasher1.prototype.start = function () {
        // this.playGame();
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    Smasher1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    Smasher1.prototype.update = function (dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    };
    Smasher1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    Smasher1.prototype.endGame = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.nightEnd.active = true;
        this.btnContinue.active = true;
        this.txtEndGame.active = true;
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.btnDownload.active = false;
    };
    Smasher1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    Smasher1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    Smasher1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var Smasher1_1;
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "btnContinue", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], Smasher1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nEndCard", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "txtEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], Smasher1.prototype, "nStart", void 0);
    Smasher1 = Smasher1_1 = __decorate([
        ccclass
    ], Smasher1);
    return Smasher1;
}(Singleton_1.default));
exports.default = Smasher1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFNTMVxcU21hc2hlcjEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsa0VBQXlEO0FBQ3pELDhDQUF5QztBQUN6QyxrREFBNkM7QUFDN0Msb0RBQStDO0FBQy9DLCtEQUEwRDtBQUVwRCxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXNDLDRCQUFtQjtJQW1EckQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFuREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGdCQUFVLEdBQVksSUFBSSxDQUFDO1FBRzNCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFFdkIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFFdkIsMEJBQW9CLEdBQVksS0FBSyxDQUFDO1FBQ3RDLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUlwQixVQUFRLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDOUIsQ0FBQztpQkF0RGdCLFFBQVE7SUF3RHpCLDJCQUFRLEdBQVI7UUFDSSwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsNEJBQVMsR0FBVDtRQUNJLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RCwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCx3QkFBSyxHQUFMO1FBQ0ksbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxpQ0FBYyxHQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFRLEVBQUU7UUFDTixJQUFHLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBQztZQUNqQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRUQsMkJBQVEsR0FBUjtRQUNJLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCwwQkFBTyxHQUFQO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUVELHdCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNEJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELDhCQUFXLEdBQVg7UUFDSSxJQUFHLGdCQUFNLENBQUMsYUFBYTtZQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQzs7SUFySUQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5Q0FDRTtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO21EQUNLO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ1M7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2lEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDUztJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNLO0lBMUNOLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0F5STVCO0lBQUQsZUFBQztDQXpJRCxBQXlJQyxDQXpJcUMsbUJBQVMsR0F5STlDO2tCQXpJb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi8uLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi8uLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNtYXNoZXIxIGV4dGVuZHMgU2luZ2xldG9uPFNtYXNoZXIxPiB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lTdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuaWdodEVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5Db250aW51ZTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBNYXA6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY291bnRFbmVteUVuZDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE15Q2hhcmFjdGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEVmZmVjdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB0eHRTbWFzaGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5FbmRDYXJkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dEVuZEdhbWU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgblN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIGJvb2xjaGVja0ludGVyYWN0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBpcm9uc291cmNlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBtaW5kd29ya3M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHZ1bmdsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgU21hc2hlcjEuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBvbkVuYWJsZSgpIHtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuYWN0aXZlR3VpZGUsIHRoaXMuYWN0aXZlR3VpZGUsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGlzYWJsZSgpIHtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQuYWN0aXZlR3VpZGUsIHRoaXMuYWN0aXZlR3VpZGUsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIC8vIHRoaXMucGxheUdhbWUoKTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5lbmVteVBhcmVudC5hY3RpdmUgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrQnRuUGxheSgpIHtcclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5TdGFydC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmKHRoaXMuY291bnRFbmVteSA9PSB0aGlzLmNvdW50RW5lbXlFbmQpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbmRHYW1lKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBsYXlHYW1lKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5nZXRDb21wb25lbnQoRW5lbXlDb250cm9sbGVyKS5TdGFydE1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZW5kR2FtZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuYnRuQ29udGludWUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnR4dEVuZEdhbWUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmpveVN0aWNrRm9sbG93LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5idG5Eb3dubG9hZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/MC2/MineCraft2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e56a2KIsLlOzqt3akB8DZIU', 'MineCraft2');
// Scripts/GamePlay/MC2/MineCraft2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var MineCraft1_1 = require("../MC1/MineCraft1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft2 = /** @class */ (function (_super) {
    __extends(MineCraft2, _super);
    function MineCraft2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1.default._instance = _this;
        return _this;
    }
    MineCraft2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.start = function () {
        this.playGame();
    };
    MineCraft2.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.EndGame();
        }
    };
    MineCraft2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft2.prototype.EndGame = function () {
        var _this = this;
        // Global.boolendG = true;
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndCard.active = true;
            _this.nEndCard.runAction(cc.fadeIn(0.5));
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nEndCard", void 0);
    MineCraft2 = __decorate([
        ccclass
    ], MineCraft2);
    return MineCraft2;
}(Singleton_1.default));
exports.default = MineCraft2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXE1DMlxcTWluZUNyYWZ0Mi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxrRUFBeUQ7QUFDekQsOENBQXlDO0FBQ3pDLGtEQUE2QztBQUM3QyxvREFBK0M7QUFDL0MsK0RBQTBEO0FBQzFELGdEQUEyQztBQUVyQyxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXdDLDhCQUFxQjtJQXdDekQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUF4Q0Qsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBRXpCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsb0JBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUNoQyxDQUFDO0lBRUQsNkJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw4QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDBCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDJCQUFNLEdBQU4sVUFBUSxFQUFFO1FBQ04sSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDN0QsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFRCw2QkFBUSxHQUFSO1FBQ0ksZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzFFO0lBQ0wsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFBQSxpQkFrQkM7UUFqQkcsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1QyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxnQ0FBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFySEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDTztJQTlCUixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBeUg5QjtJQUFELGlCQUFDO0NBekhELEFBeUhDLENBekh1QyxtQkFBUyxHQXlIaEQ7a0JBekhvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi8uLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi8uLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IE1pbmVDcmFmdDEgZnJvbSBcIi4uL01DMS9NaW5lQ3JhZnQxXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNaW5lQ3JhZnQyIGV4dGVuZHMgU2luZ2xldG9uPE1pbmVDcmFmdDE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5pZ2h0RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5FbmRDYXJkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBNaW5lQ3JhZnQxLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUdhbWUoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBFbmRHYW1lKCkge1xyXG4gICAgICAgIC8vIEdsb2JhbC5ib29sZW5kRyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5FbmRDYXJkLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZENhcmQucnVuQWN0aW9uKGNjLmZhZGVJbigwLjUpKTtcclxuICAgICAgICB9LCAxLjMpO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Random.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '698cbLh+RhKEI3RbCxjaWwp', 'Random');
// Scripts/Common/Random.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Random = /** @class */ (function (_super) {
    __extends(Random, _super);
    function Random() {
        var _this = _super.call(this) || this;
        Random_1._instance = _this;
        return _this;
    }
    Random_1 = Random;
    Random.prototype.RandomRange = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    var Random_1;
    Random = Random_1 = __decorate([
        ccclass
    ], Random);
    return Random;
}(Singleton_1.default));
exports.default = Random;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxSYW5kb20udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQW9DO0FBQzlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBb0MsMEJBQWlCO0lBQ2pEO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBREcsUUFBTSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQzVCLENBQUM7ZUFKZ0IsTUFBTTtJQUt2Qiw0QkFBVyxHQUFYLFVBQVksS0FBYSxFQUFFLEtBQWE7UUFDcEMsT0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQy9DLDZEQUE2RDtJQUNqRSxDQUFDOztJQVJnQixNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBUzFCO0lBQUQsYUFBQztDQVRELEFBU0MsQ0FUbUMsbUJBQVMsR0FTNUM7a0JBVG9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2luZ2xldG9uIGZyb20gXCIuL1NpbmdsZXRvblwiO1xyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmFuZG9tIGV4dGVuZHMgU2luZ2xldG9uPFJhbmRvbT4ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBSYW5kb20uX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxuICAgIFJhbmRvbVJhbmdlKGxvd2VyOiBudW1iZXIsIHVwcGVyOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gTWF0aC5yYW5kb20oKSAqICh1cHBlciAtIGxvd2VyKSArIGxvd2VyO1xyXG4gICAgICAgIC8vcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChsb3dlciAtIGxvd2VyKSkgKyBsb3dlcjtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnableEngine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '51ddbyV1cJCA7wv4Qd5eK5s', 'EnableEngine');
// Scripts/Common/EnableEngine.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnableEngine = /** @class */ (function (_super) {
    __extends(EnableEngine, _super);
    function EnableEngine() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EnableEngine.prototype.start = function () {
        cc.director.getPhysics3DManager().enabled = true;
        cc.director.getPhysics3DManager().gravity = cc.v3(0, 0, -300);
    };
    EnableEngine = __decorate([
        ccclass
    ], EnableEngine);
    return EnableEngine;
}(cc.Component));
exports.default = EnableEngine;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbmFibGVFbmdpbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBbUMsRUFBbEMsb0JBQU8sRUFBRSxzQkFBeUIsQ0FBQztBQUcxQztJQUEwQyxnQ0FBWTtJQUF0RDs7SUFNQSxDQUFDO0lBTEcsNEJBQUssR0FBTDtRQUNJLEVBQUUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBRWpELEVBQUUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUxnQixZQUFZO1FBRGhDLE9BQU87T0FDYSxZQUFZLENBTWhDO0lBQUQsbUJBQUM7Q0FORCxBQU1DLENBTnlDLEVBQUUsQ0FBQyxTQUFTLEdBTXJEO2tCQU5vQixZQUFZIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFbmFibGVFbmdpbmUgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgc3RhcnQoKXtcclxuICAgICAgICBjYy5kaXJlY3Rvci5nZXRQaHlzaWNzM0RNYW5hZ2VyKCkuZW5hYmxlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIGNjLmRpcmVjdG9yLmdldFBoeXNpY3MzRE1hbmFnZXIoKS5ncmF2aXR5ID0gY2MudjMoMCwwLC0zMDApO1xyXG4gICAgfVxyXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/KeyEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9d7b6zZ1X1KDK3CFBTDTqHF', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    scale: "scale",
    checkAttacked: "checkAttacked",
    plusEnemy: "plusEnemy",
    plusCamera: "plusCamera",
    activeGuide: "activeGuide"
};
exports.default = KeyEvent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxLZXlFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQU9BLElBQUksUUFBUSxHQUNaO0lBQ0ksS0FBSyxFQUFFLE9BQU87SUFDZCxhQUFhLEVBQUUsZUFBZTtJQUM5QixTQUFTLEVBQUUsV0FBVztJQUN0QixVQUFVLEVBQUUsWUFBWTtJQUN4QixXQUFXLEVBQUUsYUFBYTtDQUM3QixDQUFBO0FBQ0Qsa0JBQWUsUUFBUSxDQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEtleUV2ZW50IHtcclxuICAgIHNjYWxlOiBzdHJpbmcsXHJcbiAgICBjaGVja0F0dGFja2VkOiBzdHJpbmcsXHJcbiAgICBwbHVzRW5lbXk6IHN0cmluZyxcclxuICAgIHBsdXNDYW1lcmE6IHN0cmluZyxcclxuICAgIGFjdGl2ZUd1aWRlOiBzdHJpbmdcclxufVxyXG5sZXQgS2V5RXZlbnQ6IEtleUV2ZW50ID1cclxue1xyXG4gICAgc2NhbGU6IFwic2NhbGVcIixcclxuICAgIGNoZWNrQXR0YWNrZWQ6IFwiY2hlY2tBdHRhY2tlZFwiLFxyXG4gICAgcGx1c0VuZW15OiBcInBsdXNFbmVteVwiLFxyXG4gICAgcGx1c0NhbWVyYTogXCJwbHVzQ2FtZXJhXCIsXHJcbiAgICBhY3RpdmVHdWlkZTogXCJhY3RpdmVHdWlkZVwiXHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgS2V5RXZlbnQiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3c677+i/i1KO7mX8mMrc5uJ', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    teleport: false,
    enableAttack: false,
    boolCheckTele: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundGate: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null,
    katanaAttack: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsUUFBUSxFQUFFLEtBQUs7SUFDZixZQUFZLEVBQUUsS0FBSztJQUNuQixhQUFhLEVBQUUsS0FBSztJQUNwQixPQUFPLEVBQUUsSUFBSTtJQUNiLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLFdBQVcsRUFBRSxJQUFJO0lBQ2pCLGFBQWEsRUFBRSxJQUFJO0lBQ25CLFNBQVMsRUFBRSxJQUFJO0lBQ2YsV0FBVyxFQUFFLElBQUk7SUFDakIsU0FBUyxFQUFFLElBQUk7SUFDZixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixZQUFZLEVBQUUsSUFBSTtDQUNyQixDQUFDO0FBQ0Ysa0JBQWUsTUFBTSxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEdsb2JhbCB7XHJcbiAgICB0b3VjaFBvczogY2MuVmVjMixcclxuICAgIGJvb2xFbmFibGVUb3VjaDogYm9vbGVhbixcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRQbGF5OiBib29sZWFuLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4sXHJcbiAgICBib29sZW5kRzogYm9vbGVhbixcclxuICAgIHRlbGVwb3J0OiBib29sZWFuLFxyXG4gICAgZW5hYmxlQXR0YWNrOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrVGVsZTogYm9vbGVhbixcclxuICAgIHNvdW5kQkc6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kSW50cm86IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kQXR0YWNrOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZEZvb3RTdGVwOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZEdhdGU6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kU2NyZWFtOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFNwaW46IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kUmV3YXJkOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBrYXRhbmFBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxufVxyXG5sZXQgR2xvYmFsOiBHbG9iYWwgPSB7XHJcbiAgICB0b3VjaFBvczogbnVsbCxcclxuICAgIGJvb2xFbmFibGVUb3VjaDogZmFsc2UsXHJcbiAgICBib29sRmlyc3RUb3VjaEpveVN0aWNrOiBmYWxzZSxcclxuICAgIGJvb2xTdGFydFBsYXk6IGZhbHNlLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBmYWxzZSxcclxuICAgIGJvb2xDaGVja0F0dGFja2luZzogZmFsc2UsXHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogZmFsc2UsXHJcbiAgICBib29sZW5kRzogZmFsc2UsXHJcbiAgICB0ZWxlcG9ydDogZmFsc2UsXHJcbiAgICBlbmFibGVBdHRhY2s6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrVGVsZTogZmFsc2UsXHJcbiAgICBzb3VuZEJHOiBudWxsLFxyXG4gICAgc291bmRJbnRybzogbnVsbCxcclxuICAgIHNvdW5kQXR0YWNrOiBudWxsLFxyXG4gICAgc291bmRGb290U3RlcDogbnVsbCxcclxuICAgIHNvdW5kR2F0ZTogbnVsbCxcclxuICAgIHNvdW5kU2NyZWFtOiBudWxsLFxyXG4gICAgc291bmRTcGluOiBudWxsLFxyXG4gICAgc291bmRSZXdhcmQ6IG51bGwsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBudWxsLFxyXG4gICAga2F0YW5hQXR0YWNrOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Singleton.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b8b25AOUfxKOZOdWSle/5qU', 'Singleton');
// Scripts/Common/Singleton.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton = /** @class */ (function (_super) {
    __extends(Singleton, _super);
    function Singleton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Singleton.Instance = function (c) {
        if (this._instance == null) {
            this._instance = new c();
        }
        return this._instance;
    };
    Singleton._instance = null;
    return Singleton;
}(cc.Component));
exports.default = Singleton;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTaW5nbGV0b24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7SUFBMEMsNkJBQVk7SUFBdEQ7O0lBUUEsQ0FBQztJQVBpQixrQkFBUSxHQUF0QixVQUEwQixDQUFlO1FBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFDYSxtQkFBUyxHQUFHLElBQUksQ0FBQztJQUNuQyxnQkFBQztDQVJELEFBUUMsQ0FSeUMsRUFBRSxDQUFDLFNBQVMsR0FRckQ7a0JBUm9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2luZ2xldG9uPFQ+IGV4dGVuZHMgY2MuQ29tcG9uZW50e1xyXG4gICAgcHVibGljIHN0YXRpYyBJbnN0YW5jZTxUPihjOiB7bmV3KCk6IFQ7IH0pIDogVHtcclxuICAgICAgICBpZiAodGhpcy5faW5zdGFuY2UgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgIHRoaXMuX2luc3RhbmNlID0gbmV3IGMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlO1xyXG4gICAgfVxyXG4gICAgcHVibGljIHN0YXRpYyBfaW5zdGFuY2UgPSBudWxsO1xyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Utility.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '379e6qJyltJY6bKTs9IwXUo', 'Utility');
// Scripts/Common/Utility.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Utility = /** @class */ (function (_super) {
    __extends(Utility, _super);
    function Utility() {
        var _this = _super.call(this) || this;
        Utility_1._instance = _this;
        return _this;
    }
    Utility_1 = Utility;
    Utility.prototype.RandomRange = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    Utility.prototype.Distance = function (vec1, vec2) {
        var Distance = Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
            Math.pow(vec1.y - vec2.y, 2));
        return Distance;
    };
    var Utility_1;
    Utility = Utility_1 = __decorate([
        ccclass
    ], Utility);
    return Utility;
}(Singleton_1.default));
exports.default = Utility;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxVdGlsaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQXFDLDJCQUFrQjtJQUNuRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQURHLFNBQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUM3QixDQUFDO2dCQUpnQixPQUFPO0lBS3hCLDZCQUFXLEdBQVgsVUFBWSxLQUFhLEVBQUUsS0FBYTtRQUNwQyxPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDL0MsNkRBQTZEO0lBQ2pFLENBQUM7SUFDRCwwQkFBUSxHQUFSLFVBQVMsSUFBYSxFQUFFLElBQWE7UUFDakMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsQyxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOztJQWJnQixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBYzNCO0lBQUQsY0FBQztDQWRELEFBY0MsQ0Fkb0MsbUJBQVMsR0FjN0M7a0JBZG9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2luZ2xldG9uIGZyb20gXCIuL1NpbmdsZXRvblwiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFV0aWxpdHkgZXh0ZW5kcyBTaW5nbGV0b248VXRpbGl0eT4ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBVdGlsaXR5Ll9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcbiAgICBSYW5kb21SYW5nZShsb3dlcjogbnVtYmVyLCB1cHBlcjogbnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucmFuZG9tKCkgKiAodXBwZXIgLSBsb3dlcikgKyBsb3dlcjtcclxuICAgICAgICAvL3JldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobG93ZXIgLSBsb3dlcikpICsgbG93ZXI7XHJcbiAgICB9XHJcbiAgICBEaXN0YW5jZSh2ZWMxOiBjYy5WZWMyLCB2ZWMyOiBjYy5WZWMyKSB7XHJcbiAgICAgICAgbGV0IERpc3RhbmNlID0gTWF0aC5zcXJ0KE1hdGgucG93KHZlYzEueCAtIHZlYzIueCwgMikgK1xyXG4gICAgICAgICAgICBNYXRoLnBvdyh2ZWMxLnkgLSB2ZWMyLnksIDIpKTtcclxuICAgICAgICByZXR1cm4gRGlzdGFuY2U7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/SoundManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a465ftqP7xJ+4lT0kSrGOjc', 'SoundManager');
// Scripts/Common/SoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("./Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SoundManager = /** @class */ (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Bg = null;
        _this.footStep = null;
        _this.Intro = null;
        _this.Attack = null;
        _this.Gate = null;
        _this.Scream = null;
        _this.Spin = null;
        _this.Reward = null;
        _this.clickBtn = null;
        _this.katanaAttack = null;
        return _this;
    }
    SoundManager.prototype.onLoad = function () {
        Global_1.default.soundBG = this.Bg;
        Global_1.default.soundIntro = this.Intro;
        Global_1.default.soundFootStep = this.footStep;
        Global_1.default.soundAttack = this.Attack;
        Global_1.default.soundGate = this.Gate;
        Global_1.default.soundScream = this.Scream;
        Global_1.default.soundSpin = this.Spin;
        Global_1.default.soundReward = this.Reward;
        Global_1.default.soundClickBtn = this.clickBtn;
        Global_1.default.katanaAttack = this.katanaAttack;
    };
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Bg", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "footStep", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Intro", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Attack", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Gate", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Scream", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Spin", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Reward", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "clickBtn", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "katanaAttack", void 0);
    SoundManager = __decorate([
        ccclass
    ], SoundManager);
    return SoundManager;
}(cc.Component));
exports.default = SoundManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTb3VuZE1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBRXhCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFEdEQ7UUFBQSxxRUF3REM7UUFuREcsUUFBRSxHQUFpQixJQUFJLENBQUM7UUFJeEIsY0FBUSxHQUFpQixJQUFJLENBQUM7UUFJOUIsV0FBSyxHQUFpQixJQUFJLENBQUM7UUFJM0IsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsVUFBSSxHQUFpQixJQUFJLENBQUM7UUFJMUIsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsVUFBSSxHQUFpQixJQUFJLENBQUM7UUFJMUIsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsY0FBUSxHQUFpQixJQUFJLENBQUM7UUFLOUIsa0JBQVksR0FBaUIsSUFBSSxDQUFDOztJQWN0QyxDQUFDO0lBWkcsNkJBQU0sR0FBTjtRQUNJLGdCQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDekIsZ0JBQU0sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUMvQixnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3JDLGdCQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDakMsZ0JBQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUM3QixnQkFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2pDLGdCQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDN0IsZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3JDLGdCQUFNLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDNUMsQ0FBQztJQWxERDtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOzRDQUNzQjtJQUl4QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2tEQUM0QjtJQUk5QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOytDQUN5QjtJQUkzQjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOzhDQUN3QjtJQUkxQjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOzhDQUN3QjtJQUkxQjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2tEQUM0QjtJQUs5QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO3NEQUNnQztJQXpDakIsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQXVEaEM7SUFBRCxtQkFBQztDQXZERCxBQXVEQyxDQXZEeUMsRUFBRSxDQUFDLFNBQVMsR0F1RHJEO2tCQXZEb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBHbG9iYWwgZnJvbSBcIi4vR2xvYmFsXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU291bmRNYW5hZ2VyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgQmc6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIGZvb3RTdGVwOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBJbnRybzogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgQXR0YWNrOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBHYXRlOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBTY3JlYW06IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIFNwaW46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIFJld2FyZDogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgY2xpY2tCdG46IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBrYXRhbmFBdHRhY2s6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEJHID0gdGhpcy5CZztcclxuICAgICAgICBHbG9iYWwuc291bmRJbnRybyA9IHRoaXMuSW50cm87XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kRm9vdFN0ZXAgPSB0aGlzLmZvb3RTdGVwO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEF0dGFjayA9IHRoaXMuQXR0YWNrO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEdhdGUgPSB0aGlzLkdhdGU7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kU2NyZWFtID0gdGhpcy5TY3JlYW07XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kU3BpbiA9IHRoaXMuU3BpbjtcclxuICAgICAgICBHbG9iYWwuc291bmRSZXdhcmQgPSB0aGlzLlJld2FyZDtcclxuICAgICAgICBHbG9iYWwuc291bmRDbGlja0J0biA9IHRoaXMuY2xpY2tCdG47XHJcbiAgICAgICAgR2xvYmFsLmthdGFuYUF0dGFjayA9IHRoaXMua2F0YW5hQXR0YWNrO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/AdManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e64a5NCKE1JMag/DvwqBr33', 'AdManager');
// Scripts/Common/AdManager.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    androidLink: {
      "default": ''
    },
    iosLink: {
      "default": ''
    },
    defaultLink: {
      "default": ''
    }
  },
  openAdUrl: function openAdUrl() {
    var clickTag = '';
    window.androidLink = this.androidLink;
    window.iosLink = this.iosLink;
    window.defaultLink = this.defaultLink;

    if (window.openAdUrl) {
      window.openAdUrl();
    } else {
      window.open();
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxBZE1hbmFnZXIuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJhbmRyb2lkTGluayIsImlvc0xpbmsiLCJkZWZhdWx0TGluayIsIm9wZW5BZFVybCIsImNsaWNrVGFnIiwid2luZG93Iiwib3BlbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFdBQVcsRUFBRTtBQUNULGlCQUFTO0FBREEsS0FETDtBQUlSQyxJQUFBQSxPQUFPLEVBQUU7QUFDTCxpQkFBUztBQURKLEtBSkQ7QUFPUkMsSUFBQUEsV0FBVyxFQUFFO0FBQ1QsaUJBQVM7QUFEQTtBQVBMLEdBSFA7QUFlTEMsRUFBQUEsU0FBUyxFQUFFLHFCQUFZO0FBQ25CLFFBQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0FDLElBQUFBLE1BQU0sQ0FBQ0wsV0FBUCxHQUFxQixLQUFLQSxXQUExQjtBQUNBSyxJQUFBQSxNQUFNLENBQUNKLE9BQVAsR0FBaUIsS0FBS0EsT0FBdEI7QUFDQUksSUFBQUEsTUFBTSxDQUFDSCxXQUFQLEdBQXFCLEtBQUtBLFdBQTFCOztBQUNBLFFBQUlHLE1BQU0sQ0FBQ0YsU0FBWCxFQUFzQjtBQUNsQkUsTUFBQUEsTUFBTSxDQUFDRixTQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0hFLE1BQUFBLE1BQU0sQ0FBQ0MsSUFBUDtBQUNIO0FBQ0o7QUF6QkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBhbmRyb2lkTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW9zTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVmYXVsdExpbms6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9wZW5BZFVybDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjbGlja1RhZyA9ICcnO1xyXG4gICAgICAgIHdpbmRvdy5hbmRyb2lkTGluayA9IHRoaXMuYW5kcm9pZExpbms7XHJcbiAgICAgICAgd2luZG93Lmlvc0xpbmsgPSB0aGlzLmlvc0xpbms7XHJcbiAgICAgICAgd2luZG93LmRlZmF1bHRMaW5rID0gdGhpcy5kZWZhdWx0TGluaztcclxuICAgICAgICBpZiAod2luZG93Lm9wZW5BZFVybCkge1xyXG4gICAgICAgICAgICB3aW5kb3cub3BlbkFkVXJsKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgd2luZG93Lm9wZW4oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/VFX/BloodController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '49cb46jVzNB6KWPjpyr+e3g', 'BloodController');
// Scripts/VFX/BloodController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BloodController = /** @class */ (function (_super) {
    __extends(BloodController, _super);
    function BloodController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BloodController.prototype.onLoad = function () {
        var tween = new cc.Tween().to(0.6, { scale: 500 });
        //tween.target(this.node.children[2]).start();
        tween.target(this.node).start();
    };
    BloodController = __decorate([
        ccclass
    ], BloodController);
    return BloodController;
}(cc.Component));
exports.default = BloodController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVkZYXFxCbG9vZENvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUE2QyxtQ0FBWTtJQUF6RDs7SUFNQSxDQUFDO0lBTEcsZ0NBQU0sR0FBTjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNuRCw4Q0FBOEM7UUFDOUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUxnQixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBTW5DO0lBQUQsc0JBQUM7Q0FORCxBQU1DLENBTjRDLEVBQUUsQ0FBQyxTQUFTLEdBTXhEO2tCQU5vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJsb29kQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC42LCB7IHNjYWxlOiA1MDAgfSk7XHJcbiAgICAgICAgLy90d2Vlbi50YXJnZXQodGhpcy5ub2RlLmNoaWxkcmVuWzJdKS5zdGFydCgpO1xyXG4gICAgICAgIHR3ZWVuLnRhcmdldCh0aGlzLm5vZGUpLnN0YXJ0KCk7XHJcbiAgICB9XHJcbn0iXX0=
//------QC-SOURCE-SPLIT------
