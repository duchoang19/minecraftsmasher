
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/MC1/MineCraft1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '07cd3jvYVVF2L0lnqGm6NoC', 'MineCraft1');
// Scripts/GamePlay/MC1/MineCraft1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../../Character/CharacterController");
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft1 = /** @class */ (function (_super) {
    __extends(MineCraft1, _super);
    function MineCraft1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.btnContinue = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.nStart = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.nEnemyEnd = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1._instance = _this;
        return _this;
    }
    MineCraft1_1 = MineCraft1;
    MineCraft1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.start = function () {
        // this.playGame();
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    MineCraft1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    MineCraft1.prototype.update = function (dt) {
        var _this = this;
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    MineCraft1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft1.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    // this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    MineCraft1.prototype.EndGame = function () {
        var _this = this;
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.nEndCard.active = true;
        this.playGame();
        this.Map.active = true;
        this.nEndCard.children[2].active = true;
        this.nEndCard.children[3].active = true;
        this.nEndCard.children[4].active = true;
        for (var i = 0; i < this.nEnemyEnd.childrenCount; i++) {
            this.nEnemyEnd.children[i].getComponent(EnemyController_1.default).StartMove();
        }
        this.MyCharacter.x = 0;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 12;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 0);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.scheduleOnce(function () {
            _this.nEndCard.children[3].active = false;
        }, 5);
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var MineCraft1_1;
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnContinue", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEndCard", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nStart", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEnemyEnd", void 0);
    MineCraft1 = MineCraft1_1 = __decorate([
        ccclass
    ], MineCraft1);
    return MineCraft1;
}(Singleton_1.default));
exports.default = MineCraft1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXE1DMVxcTWluZUNyYWZ0MS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwyRUFBc0U7QUFDdEUsa0VBQXlEO0FBQ3pELDhDQUF5QztBQUN6QyxrREFBNkM7QUFDN0Msb0RBQStDO0FBQy9DLCtEQUEwRDtBQUVwRCxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXdDLDhCQUFxQjtJQTBEekQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUExREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFMUIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFDdkIsa0JBQVksR0FBWSxLQUFLLENBQUM7UUFFOUIsMEJBQW9CLEdBQVksS0FBSyxDQUFDO1FBQ3RDLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUlwQixZQUFVLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDaEMsQ0FBQzttQkE3RGdCLFVBQVU7SUErRDNCLDZCQUFRLEdBQVI7UUFDSSwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsOEJBQVMsR0FBVDtRQUNJLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RCwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQ0ksbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxtQ0FBYyxHQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsMkJBQU0sR0FBTixVQUFRLEVBQUU7UUFBVixpQkFRQztRQVBHLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUM3RCxnQkFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDO0lBRUQsNkJBQVEsR0FBUjtRQUNJLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCwrQkFBVSxHQUFWO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM5QixnQ0FBZ0M7b0JBQ2hDLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCw0QkFBTyxHQUFQO1FBQUEsaUJBbUNDO1FBbENHLGdCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDeEMsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDeEU7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDakYsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BGLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFBQSxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzdDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxnQ0FBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7O0lBbExEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7c0RBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNTO0lBRzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpREFDUTtJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1E7SUFoRFQsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQXNMOUI7SUFBRCxpQkFBQztDQXRMRCxBQXNMQyxDQXRMdUMsbUJBQVMsR0FzTGhEO2tCQXRMb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi8uLi9DaGFyYWN0ZXIvQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi8uLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi8uLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uLy4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNaW5lQ3JhZnQxIGV4dGVuZHMgU2luZ2xldG9uPE1pbmVDcmFmdDE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5pZ2h0RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkNvbnRpbnVlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFNtYXNoZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5lbXlQYXJlbnQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuRG93bmxvYWQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbkVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgblN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGNpcmNsZUVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5BbGw6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbkVuZW15RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBNaW5lQ3JhZnQxLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICAvLyB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGlja0J0blBsYXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5uU3RhcnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlN1cHBvcnRFbmQoKTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBsYXlHYW1lKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5nZXRDb21wb25lbnQoRW5lbXlDb250cm9sbGVyKS5TdGFydE1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3VwcG9ydEVuZCgpIHtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubmlnaHRFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5pZ2h0RW5kLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuTXlDaGFyYWN0ZXIub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5FbmRHYW1lKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAwLjYpO1xyXG4gICAgICAgICAgICB9LCAwLjQpO1xyXG4gICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH0sIDAuMik7XHJcbiAgICB9XHJcblxyXG4gICAgRW5kR2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbGVuZEcgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5uRW5kQ2FyZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMucGxheUdhbWUoKTtcclxuICAgICAgICB0aGlzLk1hcC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubkVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm5FbmRDYXJkLmNoaWxkcmVuWzNdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5uRW5kQ2FyZC5jaGlsZHJlbls0XS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLm5FbmVteUVuZC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5uRW5lbXlFbmQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIueCA9IDA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci55ID0gLTYwO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuc2NhbGUgPSAxMjtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoOTAsIDAsIDApO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuYm9keURlYXRoLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmFjdGl2ZSA9IGZhbHNlOztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5idG5BbGwuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfSwgNSlcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19