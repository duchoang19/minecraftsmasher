
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/MC2/MineCraft2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e56a2KIsLlOzqt3akB8DZIU', 'MineCraft2');
// Scripts/GamePlay/MC2/MineCraft2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var MineCraft1_1 = require("../MC1/MineCraft1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft2 = /** @class */ (function (_super) {
    __extends(MineCraft2, _super);
    function MineCraft2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1.default._instance = _this;
        return _this;
    }
    MineCraft2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.start = function () {
        this.playGame();
    };
    MineCraft2.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.EndGame();
        }
    };
    MineCraft2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft2.prototype.EndGame = function () {
        var _this = this;
        // Global.boolendG = true;
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndCard.active = true;
            _this.nEndCard.runAction(cc.fadeIn(0.5));
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nEndCard", void 0);
    MineCraft2 = __decorate([
        ccclass
    ], MineCraft2);
    return MineCraft2;
}(Singleton_1.default));
exports.default = MineCraft2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXE1DMlxcTWluZUNyYWZ0Mi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxrRUFBeUQ7QUFDekQsOENBQXlDO0FBQ3pDLGtEQUE2QztBQUM3QyxvREFBK0M7QUFDL0MsK0RBQTBEO0FBQzFELGdEQUEyQztBQUVyQyxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXdDLDhCQUFxQjtJQXdDekQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUF4Q0Qsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBRXpCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsb0JBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUNoQyxDQUFDO0lBRUQsNkJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw4QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDBCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDJCQUFNLEdBQU4sVUFBUSxFQUFFO1FBQ04sSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDN0QsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFRCw2QkFBUSxHQUFSO1FBQ0ksZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzFFO0lBQ0wsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFBQSxpQkFrQkM7UUFqQkcsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1QyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCwwQkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDhCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxnQ0FBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFySEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDTztJQTlCUixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBeUg5QjtJQUFELGlCQUFDO0NBekhELEFBeUhDLENBekh1QyxtQkFBUyxHQXlIaEQ7a0JBekhvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uLy4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi8uLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi8uLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IE1pbmVDcmFmdDEgZnJvbSBcIi4uL01DMS9NaW5lQ3JhZnQxXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNaW5lQ3JhZnQyIGV4dGVuZHMgU2luZ2xldG9uPE1pbmVDcmFmdDE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5pZ2h0RW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5FbmRDYXJkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBNaW5lQ3JhZnQxLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUdhbWUoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBFbmRHYW1lKCkge1xyXG4gICAgICAgIC8vIEdsb2JhbC5ib29sZW5kRyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5FbmRDYXJkLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZENhcmQucnVuQWN0aW9uKGNjLmZhZGVJbigwLjUpKTtcclxuICAgICAgICB9LCAxLjMpO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19