
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Character/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8c229TU8nVEg7patjhCarLi', 'CharacterController');
// Scripts/Character/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.JoystickFollow = null;
        _this.actionType = EnumDefine_1.ActionType.IDLE;
        _this.speed = 250;
        _this.Weapon = null;
        _this.timeAnim = 0;
        _this.placeBua = null;
        _this.effectSmoke = null;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampBottom = 0;
        _this.clampTop = 0;
        _this.level = 0;
        _this.scale = 0;
        _this.originalSpeed = 0;
        _this.attack = false;
        _this.gameplayInstance = null;
        _this.boolPlaySoundFoot = false;
        return _this;
    }
    // onLoad () {}
    CharacterController.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        Global_1.default.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var physic_mamanger = cc.director.getPhysics3DManager();
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
        physic_mamanger.enabled = true;
        physic_mamanger.gravity = cc.v3(0, 0, -3000);
        this.collider = this.getComponent(cc.Collider3D);
    };
    CharacterController.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !Global_1.default.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            var PosForX = this.node.getPosition();
            var PosForY = this.node.getPosition();
            PosForX.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
                this.scheduleOnce(function () {
                    _this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global_1.default.touchPos.y, Global_1.default.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        Global_1.default.boolStartAttacking = true;
        Global_1.default.boolCheckAttacking = false;
        // this.Hand.active = true;
        this.Weapon.active = true;
        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_attack");
        }
        // if(!Global.teleport) {
        //     cc.log('=======' , this.attack)
        //     this.attack = true;
        // } else {
        //     this.attack = false;
        // }
        this.scheduleOnce(function () {
            Global_1.default.boolCheckAttacking = true;
            Global_1.default.boolCheckAttacked = true;
            // this.spawnEffectSmoke(this.effectSmoke);
            if (_this.node.name == "MyDeadpool") {
                cc.audioEngine.playEffect(Global_1.default.katanaAttack, false);
            }
            else {
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
            }
        }, 0.5);
        // if(Global.teleport) {
        //     this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        // }
        this.scheduleOnce(function () {
            GamePlayInstance_1.instance.emit(KeyEvent_1.default.activeGuide);
            Global_1.default.boolStartAttacking = false;
            if (_this.node.name == "MyDeadpool") {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_idle");
            }
            else {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            }
            // this.Hand.active = false;
            // this.Weapon.active = false;
        }, this.timeAnim);
    };
    CharacterController.prototype.LevelUpPlayer = function () {
        this.node.scaleX = this.node.scaleX + this.scale;
        this.node.scaleY = this.node.scaleY + this.scale;
        this.node.scaleZ = this.node.scaleZ + this.scale;
        if (this.level < 4)
            this.level++;
    };
    CharacterController.prototype.spawnEffectSmoke = function (smoke) {
        var smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeBua.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "JoystickFollow", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.ActionType) })
    ], CharacterController.prototype, "actionType", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speed", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "Weapon", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeBua", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "effectSmoke", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "level", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "scale", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2hhcmFjdGVyXFxDaGFyYWN0ZXJDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG1EQUFrRDtBQUNsRCwrREFBd0U7QUFDeEUsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUVwQyxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBRzFDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBZ0tDO1FBNUpHLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLGdCQUFVLEdBQWUsdUJBQVUsQ0FBQyxJQUFJLENBQUM7UUFHekMsV0FBSyxHQUFXLEdBQUcsQ0FBQztRQUdwQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixpQkFBVyxHQUFjLElBQUksQ0FBQztRQUc5QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBR3RCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBR3ZCLGlCQUFXLEdBQVcsQ0FBQyxDQUFDO1FBR3hCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUdsQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBS2xCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFFeEIsc0JBQWdCLEdBQXFCLElBQUksQ0FBQztRQUMxQyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7O0lBNEd2QyxDQUFDO0lBMUdHLGVBQWU7SUFFZixtQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BFLGdCQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RELGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO1FBQzlCLGVBQWUsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsb0NBQU0sR0FBTjtRQUFBLGlCQXVCQztRQXRCRyxJQUFHLGdCQUFNLENBQUMsZUFBZSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUNyRCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzRSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3RDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDdEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztnQkFDbkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ1g7WUFDRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6RCxJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pDLE1BQU0sR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUN6RDtJQUNMLENBQUM7SUFFRCx1Q0FBUyxHQUFUO1FBQUEsaUJBZ0RDO1FBL0NHLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFFMUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLEVBQUU7WUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLENBQUM7U0FDeEY7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1NBQ3RGO1FBRUQseUJBQXlCO1FBQ3pCLHNDQUFzQztRQUN0QywwQkFBMEI7UUFDMUIsV0FBVztRQUNYLDJCQUEyQjtRQUMzQixJQUFJO1FBRUosSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLDJDQUEyQztZQUMzQyxJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtnQkFDaEMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0gsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDeEQ7UUFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFUix3QkFBd0I7UUFDeEIsMEVBQTBFO1FBQzFFLElBQUk7UUFFSixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNwQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUVsQyxJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtnQkFDaEMsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7YUFDdEY7aUJBQ0k7Z0JBQ0QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7YUFDcEY7WUFDRCw0QkFBNEI7WUFDNUIsOEJBQThCO1FBQ2xDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELDJDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNqRCxJQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQztZQUNiLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsOENBQWdCLEdBQWhCLFVBQWlCLEtBQWdCO1FBQzdCLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsR0FBRyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDckMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDdkUsR0FBRyxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4RCxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNkLENBQUM7SUEzSkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytEQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQVUsQ0FBQyxFQUFFLENBQUM7MkRBQ0M7SUFHekM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDRDtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3VEQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7eURBQ0E7SUFHckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzREQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MERBQ0M7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzsyREFDRTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzREQUNHO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7eURBQ0E7SUFHckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDSDtJQUdsQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3NEQUNIO0lBMUNELG1CQUFtQjtRQUR2QyxPQUFPO09BQ2EsbUJBQW1CLENBK0p2QztJQUFELDBCQUFDO0NBL0pELEFBK0pDLENBL0pnRCxFQUFFLENBQUMsU0FBUyxHQStKNUQ7a0JBL0pvQixtQkFBbUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3Rpb25UeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhcmFjdGVyQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBKb3lzdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShBY3Rpb25UeXBlKSB9KVxyXG4gICAgYWN0aW9uVHlwZTogQWN0aW9uVHlwZSA9IEFjdGlvblR5cGUuSURMRTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkOiBudW1iZXIgPSAyNTA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBXZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltZUFuaW06IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZUJ1YTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcclxuICAgIGVmZmVjdFNtb2tlOiBjYy5QcmVmYWIgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBMZWZ0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBSaWdodDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wQm90dG9tOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBsZXZlbDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNjYWxlOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIHJpZ2lkYm9keTogY2MuUmlnaWRCb2R5M0Q7XHJcbiAgICBjb2xsaWRlcjogY2MuQ29sbGlkZXIzRDtcclxuXHJcbiAgICBvcmlnaW5hbFNwZWVkOiBudW1iZXIgPSAwO1xyXG4gICAgYXR0YWNrOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZSA9IG51bGw7XHJcbiAgICBib29sUGxheVNvdW5kRm9vdDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8vIG9uTG9hZCAoKSB7fVxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UgPSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpO1xyXG4gICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IGNjLnYyKDAsIDApO1xyXG4gICAgICAgIHRoaXMub3JpZ2luYWxTcGVlZCA9IHRoaXMuc3BlZWQ7XHJcbiAgICAgICAgdGhpcy5yaWdpZGJvZHkgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKTtcclxuICAgICAgICBsZXQgcGh5c2ljX21hbWFuZ2VyID0gY2MuZGlyZWN0b3IuZ2V0UGh5c2ljczNETWFuYWdlcigpO1xyXG4gICAgICAgIHRoaXMucmlnaWRib2R5LnNldExpbmVhclZlbG9jaXR5KG5ldyBjYy5WZWMzKDAsMCwgMCkpO1xyXG4gICAgICAgIHBoeXNpY19tYW1hbmdlci5lbmFibGVkID0gdHJ1ZVxyXG4gICAgICAgIHBoeXNpY19tYW1hbmdlci5ncmF2aXR5ID0gY2MudjMoMCwgMCwgLTMwMDApO1xyXG4gICAgICAgIHRoaXMuY29sbGlkZXIgPSB0aGlzLmdldENvbXBvbmVudChjYy5Db2xsaWRlcjNEKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCB0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueSwgdGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIGxldCBQb3NGb3JYID0gdGhpcy5ub2RlLmdldFBvc2l0aW9uKCk7XHJcbiAgICAgICAgICAgIGxldCBQb3NGb3JZID0gdGhpcy5ub2RlLmdldFBvc2l0aW9uKCk7XHJcbiAgICAgICAgICAgIFBvc0ZvclguYWRkU2VsZihHbG9iYWwudG91Y2hQb3MubXVsKHRoaXMuc3BlZWQgLyAxMDApKTtcclxuICAgICAgICAgICAgUG9zRm9yWS5hZGRTZWxmKEdsb2JhbC50b3VjaFBvcy5tdWwodGhpcy5zcGVlZCAvIDEwMCkpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IFBvc0ZvclgueDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBQb3NGb3JZLnk7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5ib29sUGxheVNvdW5kRm9vdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sUGxheVNvdW5kRm9vdCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEZvb3RTdGVwLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ib29sUGxheVNvdW5kRm9vdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSwgMC4zKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgciA9IE1hdGguYXRhbjIoR2xvYmFsLnRvdWNoUG9zLnksIEdsb2JhbC50b3VjaFBvcy54KTtcclxuICAgICAgICAgICAgdmFyIGRlZ3JlZSA9IHIgKiAxODAgLyAoTWF0aC5QSSk7XHJcbiAgICAgICAgICAgIGRlZ3JlZSA9IDM2MCAtIGRlZ3JlZSArIDkwO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuaXMzRE5vZGUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMygtOTAsIDE4MCwgZGVncmVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQXR0YWNraW5nKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAvLyB0aGlzLkhhbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLldlYXBvbi5hY3RpdmUgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5ub2RlLm5hbWUgPT0gXCJNeURlYWRwb29sXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcImNoYXJhY3Rlcl9ib25lc3xhc3Nhc3Npbl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZighR2xvYmFsLnRlbGVwb3J0KSB7XHJcbiAgICAgICAgLy8gICAgIGNjLmxvZygnPT09PT09PScgLCB0aGlzLmF0dGFjaylcclxuICAgICAgICAvLyAgICAgdGhpcy5hdHRhY2sgPSB0cnVlO1xyXG4gICAgICAgIC8vIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMuYXR0YWNrID0gZmFsc2U7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnNwYXduRWZmZWN0U21va2UodGhpcy5lZmZlY3RTbW9rZSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15RGVhZHBvb2xcIikge1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwua2F0YW5hQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEF0dGFjaywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMC41KTtcclxuXHJcbiAgICAgICAgLy8gaWYoR2xvYmFsLnRlbGVwb3J0KSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ19VU19pZGxlXCIpO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LmFjdGl2ZUd1aWRlKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKHRoaXMubm9kZS5uYW1lID09IFwiTXlEZWFkcG9vbFwiKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGFzc2Fzc2luX2lkbGVcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHRoaXMuSGFuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gdGhpcy5XZWFwb24uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICB9XHJcblxyXG4gICAgTGV2ZWxVcFBsYXllcigpIHtcclxuICAgICAgICB0aGlzLm5vZGUuc2NhbGVYID0gdGhpcy5ub2RlLnNjYWxlWCArIHRoaXMuc2NhbGU7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNjYWxlWSA9IHRoaXMubm9kZS5zY2FsZVkgKyB0aGlzLnNjYWxlO1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZVogPSB0aGlzLm5vZGUuc2NhbGVaICsgdGhpcy5zY2FsZTtcclxuICAgICAgICBpZih0aGlzLmxldmVsIDwgNClcclxuICAgICAgICAgICAgdGhpcy5sZXZlbCsrO1xyXG4gICAgfVxyXG5cclxuICAgIHNwYXduRWZmZWN0U21va2Uoc21va2U6IGNjLlByZWZhYikge1xyXG4gICAgICAgIGxldCBzbWsgPSBjYy5pbnN0YW50aWF0ZShzbW9rZSk7XHJcbiAgICAgICAgc21rLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIGxldCBwb3MgPSB0aGlzLm5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKHRoaXMucGxhY2VCdWEuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgcG9zID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIocG9zKTtcclxuICAgICAgICBzbWsueCA9IHBvcy54O1xyXG4gICAgICAgIHNtay55ID0gcG9zLnk7XHJcbiAgICAgICAgc21rLnogPSAwO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==