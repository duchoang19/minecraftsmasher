
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Camera/CameraFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a3c4BNdsNGOKf91Tc0+3pS', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.gameplayInstance = null;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        _this.Target = null;
        //9,12
        _this.plusY = 0;
        _this.plusZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 43;
        this.cameraOffsetZ = this.node.z;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay && !Global_1.default.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if (Global_1.default.boolendG) {
            cc.log('true');
            this.node.x = 0;
            this.node.y = -124;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38, 0, 0);
        }
        // else if (!Global.teleport) {
        //     Global.boolCheckTele = true;
        //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX + 20, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY + 20, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ - 50, 0.2);
        //     // this.node.x = -87;
        //     // this.node.y = 60;
        //     // this.node.z = 76.5;
        //     this.node.eulerAngles = new cc.Vec3(45.5, -2, 22);
        // }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    };
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    CameraFollow.prototype.PlusYZ = function () {
        this.plusY += 12;
        this.plusZ += 9;
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2FtZXJhXFxDYW1lcmFGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUR0RDtRQUFBLHFFQTBHQztRQXhHRyxzQkFBZ0IsR0FBcUIsSUFBSSxDQUFDO1FBQzFDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsTUFBTTtRQUNOLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQzs7SUFpR3RCLENBQUM7SUFoR0csNEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFDRCw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsUUFBUSxFQUFFO1lBQzFDLGtGQUFrRjtZQUNsRixrRkFBa0Y7WUFDbEYseUJBQXlCO1lBQ3pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDekYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNuSDtZQUNELHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osc0dBQXNHO1lBQ3RHLHVIQUF1SDtZQUN2SCxvSUFBb0k7WUFDcEksb0lBQW9JO1lBQ3BJLElBQUk7WUFDSixzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osR0FBRztpQkFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLDZCQUE2QjtnQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3ZIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO2lCQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDOUYsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO1NBQ0o7YUFDSSxJQUFHLGdCQUFNLENBQUMsUUFBUSxFQUFDO1lBQ3BCLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsK0JBQStCO1FBQy9CLG1DQUFtQztRQUNuQyw0SEFBNEg7UUFDNUgsNEhBQTRIO1FBQzVILDRIQUE0SDtRQUM1SCw0QkFBNEI7UUFDNUIsMkJBQTJCO1FBQzNCLDZCQUE2QjtRQUM3Qix5REFBeUQ7UUFDekQsSUFBSTtRQUNKLFNBQVM7UUFDVCwyRkFBMkY7UUFDM0YsZ0ZBQWdGO1FBQ2hGLFFBQVE7UUFDUiwwQkFBMEI7UUFDMUIsdUVBQXVFO1FBQ3ZFLHdGQUF3RjtRQUN4Rix3RkFBd0Y7UUFDeEYsR0FBRztRQUNILEdBQUc7SUFDUCxDQUFDO0lBQ0QsaURBQWlEO0lBQ2pELG1EQUFtRDtJQUNuRCxtREFBbUQ7SUFDbkQsbURBQW1EO0lBQ25ELElBQUk7SUFDSiw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQXhHZ0IsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQXlHaEM7SUFBRCxtQkFBQztDQXpHRCxBQXlHQyxDQXpHeUMsRUFBRSxDQUFDLFNBQVMsR0F5R3JEO2tCQXpHb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlIGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FtZXJhRm9sbG93IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIGdhbWVwbGF5SW5zdGFuY2U6IEdhbWVQbGF5SW5zdGFuY2UgPSBudWxsO1xyXG4gICAgY2FtZXJhT2Zmc2V0WDogbnVtYmVyID0gMDtcclxuICAgIGNhbWVyYU9mZnNldFk6IG51bWJlciA9IDA7XHJcbiAgICBjYW1lcmFPZmZzZXRaOiBudW1iZXIgPSAwO1xyXG4gICAgVGFyZ2V0OiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIC8vOSwxMlxyXG4gICAgcGx1c1k6IG51bWJlciA9IDA7XHJcbiAgICBwbHVzWjogbnVtYmVyID0gMDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSk7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRYID0gdGhpcy5ub2RlLnggLSAwO1xyXG4gICAgICAgIHRoaXMuY2FtZXJhT2Zmc2V0WSA9IHRoaXMubm9kZS55ICsgNDM7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRaID0gdGhpcy5ub2RlLno7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgLy9sZXQgbmV3UG9zWCA9IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYO1xyXG4gICAgICAgICAgICAvL2xldCBuZXdQb3NaID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFo7XHJcbiAgICAgICAgICAgIC8vaWYgKCFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaLCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy8gZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAyKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSB0aGlzLnBsdXNZLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgdGhpcy5wbHVzWiwgMC4yKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAvLyBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDMpIHtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHRoaXMucGx1c1ksIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyB0aGlzLnBsdXNaLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy99XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnJlc2V0T2Zmc2V0KDAsIDEyLCA5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDEyLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgOSwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDI0LCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgMTgsIDAuMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSAzNiwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIDI3LCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gNDgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyAzNiwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKEdsb2JhbC5ib29sZW5kRyl7XHJcbiAgICAgICAgICAgIGNjLmxvZygndHJ1ZScpXHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSAtMTI0O1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueiA9IDEyMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMzgsMCwwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZWxzZSBpZiAoIUdsb2JhbC50ZWxlcG9ydCkge1xyXG4gICAgICAgIC8vICAgICBHbG9iYWwuYm9vbENoZWNrVGVsZSA9IHRydWU7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCArIDIwLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgKyAyMCwgMC4yKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaIC0gNTAsIDAuMik7XHJcbiAgICAgICAgLy8gICAgIC8vIHRoaXMubm9kZS54ID0gLTg3O1xyXG4gICAgICAgIC8vICAgICAvLyB0aGlzLm5vZGUueSA9IDYwO1xyXG4gICAgICAgIC8vICAgICAvLyB0aGlzLm5vZGUueiA9IDc2LjU7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKDQ1LjUsIC0yLCAyMik7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vIGVsc2Uge1xyXG4gICAgICAgIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuVGFyZ2V0ID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5LmVuZW15UGFyZW50LmNoaWxkcmVuW2ldO1xyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gICAgIHRoaXMucmVzZXRPZmZzZXQoKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuVGFyZ2V0LnggLyAyLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5UYXJnZXQueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSwgMC4yKTtcclxuICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuVGFyZ2V0LnogKyB0aGlzLmNhbWVyYU9mZnNldFosIDAuMik7XHJcbiAgICAgICAgLy99XHJcbiAgICAgICAgLy99XHJcbiAgICB9XHJcbiAgICAvLyByZXNldE9mZnNldCh4OiBudW1iZXIsIHk6IG51bWJlciwgejogbnVtYmVyKSB7XHJcbiAgICAvLyAgICAgdGhpcy5jYW1lcmFPZmZzZXRYID0gdGhpcy5jYW1lcmFPZmZzZXRYIC0geDtcclxuICAgIC8vICAgICB0aGlzLmNhbWVyYU9mZnNldFkgPSB0aGlzLmNhbWVyYU9mZnNldFkgLSB5O1xyXG4gICAgLy8gICAgIHRoaXMuY2FtZXJhT2Zmc2V0WiA9IHRoaXMuY2FtZXJhT2Zmc2V0WiArIHo7XHJcbiAgICAvLyB9XHJcbiAgICBQbHVzWVooKSB7XHJcbiAgICAgICAgdGhpcy5wbHVzWSArPSAxMjtcclxuICAgICAgICB0aGlzLnBsdXNaICs9IDk7XHJcbiAgICB9XHJcbn1cclxuIl19