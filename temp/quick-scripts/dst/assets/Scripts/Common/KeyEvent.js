
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/KeyEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9d7b6zZ1X1KDK3CFBTDTqHF', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    scale: "scale",
    checkAttacked: "checkAttacked",
    plusEnemy: "plusEnemy",
    plusCamera: "plusCamera",
    activeGuide: "activeGuide"
};
exports.default = KeyEvent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxLZXlFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQU9BLElBQUksUUFBUSxHQUNaO0lBQ0ksS0FBSyxFQUFFLE9BQU87SUFDZCxhQUFhLEVBQUUsZUFBZTtJQUM5QixTQUFTLEVBQUUsV0FBVztJQUN0QixVQUFVLEVBQUUsWUFBWTtJQUN4QixXQUFXLEVBQUUsYUFBYTtDQUM3QixDQUFBO0FBQ0Qsa0JBQWUsUUFBUSxDQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEtleUV2ZW50IHtcclxuICAgIHNjYWxlOiBzdHJpbmcsXHJcbiAgICBjaGVja0F0dGFja2VkOiBzdHJpbmcsXHJcbiAgICBwbHVzRW5lbXk6IHN0cmluZyxcclxuICAgIHBsdXNDYW1lcmE6IHN0cmluZyxcclxuICAgIGFjdGl2ZUd1aWRlOiBzdHJpbmdcclxufVxyXG5sZXQgS2V5RXZlbnQ6IEtleUV2ZW50ID1cclxue1xyXG4gICAgc2NhbGU6IFwic2NhbGVcIixcclxuICAgIGNoZWNrQXR0YWNrZWQ6IFwiY2hlY2tBdHRhY2tlZFwiLFxyXG4gICAgcGx1c0VuZW15OiBcInBsdXNFbmVteVwiLFxyXG4gICAgcGx1c0NhbWVyYTogXCJwbHVzQ2FtZXJhXCIsXHJcbiAgICBhY3RpdmVHdWlkZTogXCJhY3RpdmVHdWlkZVwiXHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgS2V5RXZlbnQiXX0=