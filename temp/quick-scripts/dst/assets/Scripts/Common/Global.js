
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3c677+i/i1KO7mX8mMrc5uJ', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    teleport: false,
    enableAttack: false,
    boolCheckTele: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundGate: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null,
    katanaAttack: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsUUFBUSxFQUFFLEtBQUs7SUFDZixZQUFZLEVBQUUsS0FBSztJQUNuQixhQUFhLEVBQUUsS0FBSztJQUNwQixPQUFPLEVBQUUsSUFBSTtJQUNiLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLFdBQVcsRUFBRSxJQUFJO0lBQ2pCLGFBQWEsRUFBRSxJQUFJO0lBQ25CLFNBQVMsRUFBRSxJQUFJO0lBQ2YsV0FBVyxFQUFFLElBQUk7SUFDakIsU0FBUyxFQUFFLElBQUk7SUFDZixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixZQUFZLEVBQUUsSUFBSTtDQUNyQixDQUFDO0FBQ0Ysa0JBQWUsTUFBTSxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEdsb2JhbCB7XHJcbiAgICB0b3VjaFBvczogY2MuVmVjMixcclxuICAgIGJvb2xFbmFibGVUb3VjaDogYm9vbGVhbixcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRQbGF5OiBib29sZWFuLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4sXHJcbiAgICBib29sZW5kRzogYm9vbGVhbixcclxuICAgIHRlbGVwb3J0OiBib29sZWFuLFxyXG4gICAgZW5hYmxlQXR0YWNrOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrVGVsZTogYm9vbGVhbixcclxuICAgIHNvdW5kQkc6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kSW50cm86IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kQXR0YWNrOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZEZvb3RTdGVwOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZEdhdGU6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kU2NyZWFtOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFNwaW46IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kUmV3YXJkOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBrYXRhbmFBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxufVxyXG5sZXQgR2xvYmFsOiBHbG9iYWwgPSB7XHJcbiAgICB0b3VjaFBvczogbnVsbCxcclxuICAgIGJvb2xFbmFibGVUb3VjaDogZmFsc2UsXHJcbiAgICBib29sRmlyc3RUb3VjaEpveVN0aWNrOiBmYWxzZSxcclxuICAgIGJvb2xTdGFydFBsYXk6IGZhbHNlLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBmYWxzZSxcclxuICAgIGJvb2xDaGVja0F0dGFja2luZzogZmFsc2UsXHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogZmFsc2UsXHJcbiAgICBib29sZW5kRzogZmFsc2UsXHJcbiAgICB0ZWxlcG9ydDogZmFsc2UsXHJcbiAgICBlbmFibGVBdHRhY2s6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrVGVsZTogZmFsc2UsXHJcbiAgICBzb3VuZEJHOiBudWxsLFxyXG4gICAgc291bmRJbnRybzogbnVsbCxcclxuICAgIHNvdW5kQXR0YWNrOiBudWxsLFxyXG4gICAgc291bmRGb290U3RlcDogbnVsbCxcclxuICAgIHNvdW5kR2F0ZTogbnVsbCxcclxuICAgIHNvdW5kU2NyZWFtOiBudWxsLFxyXG4gICAgc291bmRTcGluOiBudWxsLFxyXG4gICAgc291bmRSZXdhcmQ6IG51bGwsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBudWxsLFxyXG4gICAga2F0YW5hQXR0YWNrOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=