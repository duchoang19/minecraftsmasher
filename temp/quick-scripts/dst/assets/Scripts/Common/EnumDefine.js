
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnumDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf993Azr2BF7p40pHHz6qKS', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionType;
(function (ActionType) {
    ActionType[ActionType["IDLE"] = 0] = "IDLE";
    ActionType[ActionType["RUN"] = 1] = "RUN";
    ActionType[ActionType["ATTACK"] = 2] = "ATTACK";
    ActionType[ActionType["VICTORY"] = 3] = "VICTORY";
    ActionType[ActionType["SCARE"] = 4] = "SCARE";
    ActionType[ActionType["NORMAL"] = 5] = "NORMAL";
    ActionType[ActionType["FAST"] = 6] = "FAST";
})(ActionType = exports.ActionType || (exports.ActionType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbnVtRGVmaW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBWSxVQVNYO0FBVEQsV0FBWSxVQUFVO0lBRWxCLDJDQUFJLENBQUE7SUFDSix5Q0FBRyxDQUFBO0lBQ0gsK0NBQU0sQ0FBQTtJQUNOLGlEQUFPLENBQUE7SUFDUCw2Q0FBSyxDQUFBO0lBQ0wsK0NBQU0sQ0FBQTtJQUNOLDJDQUFJLENBQUE7QUFDUixDQUFDLEVBVFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFTckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBBY3Rpb25UeXBlXHJcbntcclxuICAgIElETEUsXHJcbiAgICBSVU4sXHJcbiAgICBBVFRBQ0ssXHJcbiAgICBWSUNUT1JZLFxyXG4gICAgU0NBUkUsXHJcbiAgICBOT1JNQUwsXHJcbiAgICBGQVNULFxyXG59Il19