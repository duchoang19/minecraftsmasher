
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Vent/VentCtrl.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '47feeRjYpxKSKMq10SbyDma', 'VentCtrl');
// Scripts/Vent/VentCtrl.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var VentCtrl = /** @class */ (function (_super) {
    __extends(VentCtrl, _super);
    function VentCtrl() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.xMin = 0;
        _this.xMax = 0;
        _this.yMin = 0;
        _this.yMax = 0;
        _this.vent2 = null;
        _this.boolCheckTele = false;
        return _this;
    }
    VentCtrl.prototype.onLoad = function () { };
    VentCtrl.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
    };
    VentCtrl.prototype.update = function (dt) {
        var _this = this;
        var character = this.gameplayInstance.gameplay.MyCharacter;
        if (this.xMin < character.x && this.xMax > character.x && this.yMin < character.y && this.yMax > character.y && !Global_1.default.boolCheckTele) {
            Global_1.default.boolCheckTele = true;
            Global_1.default.enableAttack = false;
            Global_1.default.boolEnableTouch = false;
            character.getComponent(CharacterController_1.default).enabled = false;
            character.getComponent(cc.SkeletonAnimation).play('Among_US_idle');
            this.node.getComponent(cc.Animation).play('Vent');
            character.getComponent(CharacterController_1.default).JoystickFollow.active = false;
            character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
            var tween = new cc.Tween().to(0.3, { position: cc.v3(this.node.x + 2, this.node.y, this.node.z + 10) }).to(0.65, { scale: 0 });
            tween.target(character).start();
            this.scheduleOnce(function () {
                Global_1.default.boolendG = true;
                Global_1.default.boolStartAttacking = false;
                var posVent2 = _this.vent2.getPosition();
                _this.vent2.getComponent(cc.Animation).play('Vent');
                var tween = new cc.Tween().to(0.5, { position: cc.v3(posVent2.x, posVent2.y, 20) }, { easing: 'easeBackIn' }).to(0.5, { scale: 500 }).call(function () {
                });
                tween.target(character).start();
                character.setPosition(cc.v3(posVent2.x + 10, posVent2.y - 40, -15));
                character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
                character.z = 55;
                character.scale = 500;
                character.angle = 80;
                character.eulerAngles = new cc.Vec3(-90, 180, -24);
            }, 0.8);
        }
    };
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMax", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMax", void 0);
    __decorate([
        property(cc.Node)
    ], VentCtrl.prototype, "vent2", void 0);
    VentCtrl = __decorate([
        ccclass
    ], VentCtrl);
    return VentCtrl;
}(cc.Component));
exports.default = VentCtrl;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVmVudFxcVmVudEN0cmwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUFzQyw0QkFBWTtJQURsRDtRQUFBLHFFQXlEQztRQXJERyxVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBR2pCLFVBQUksR0FBVyxDQUFDLENBQUM7UUFHakIsVUFBSSxHQUFXLENBQUMsQ0FBQztRQUdqQixVQUFJLEdBQVcsQ0FBQyxDQUFDO1FBR2pCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsbUJBQWEsR0FBWSxLQUFLLENBQUM7O0lBc0NuQyxDQUFDO0lBcENHLHlCQUFNLEdBQU4sY0FBVyxDQUFDO0lBRVosd0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFPLEVBQUU7UUFBVCxpQkE2QkM7UUE1QkcsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFDM0QsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLGFBQWEsRUFBRTtZQUNuSSxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDNUIsZ0JBQU0sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzVCLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUMvQixTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUM1RCxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2xELFNBQVMsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUMxRSxTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDMUUsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdILEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2dCQUNsQyxJQUFJLFFBQVEsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN4QyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNuRCxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQyxNQUFNLEVBQUUsWUFBWSxFQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN6SSxDQUFDLENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNoQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNwRSxTQUFTLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQzFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNqQixTQUFTLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztnQkFDdEIsU0FBUyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtTQUNWO0lBQ0wsQ0FBQztJQXBERDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBDQUNKO0lBR2pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MENBQ0o7SUFHakI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzswQ0FDSjtJQUdqQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBDQUNKO0lBR2pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MkNBQ0k7SUFmTCxRQUFRO1FBRDVCLE9BQU87T0FDYSxRQUFRLENBd0Q1QjtJQUFELGVBQUM7Q0F4REQsQUF3REMsQ0F4RHFDLEVBQUUsQ0FBQyxTQUFTLEdBd0RqRDtrQkF4RG9CLFFBQVEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWZW50Q3RybCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICB4TWluOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgeE1heDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHlNaW46IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICB5TWF4OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdmVudDI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGdhbWVwbGF5SW5zdGFuY2U6IEdhbWVQbGF5SW5zdGFuY2U7XHJcbiAgICBib29sQ2hlY2tUZWxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgb25Mb2FkKCkgeyB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoZHQpIHtcclxuICAgICAgICBsZXQgY2hhcmFjdGVyID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyO1xyXG4gICAgICAgIGlmICh0aGlzLnhNaW4gPCBjaGFyYWN0ZXIueCAmJiB0aGlzLnhNYXggPiBjaGFyYWN0ZXIueCAmJiB0aGlzLnlNaW4gPCBjaGFyYWN0ZXIueSAmJiB0aGlzLnlNYXggPiBjaGFyYWN0ZXIueSAmJiAhR2xvYmFsLmJvb2xDaGVja1RlbGUpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja1RlbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuZW5hYmxlQXR0YWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGNoYXJhY3Rlci5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoJ0Ftb25nX1VTX2lkbGUnKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5BbmltYXRpb24pLnBsYXkoJ1ZlbnQnKTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5Kb3lzdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC4zLCB7cG9zaXRpb246IGNjLnYzKHRoaXMubm9kZS54ICsgMiwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56ICsgMTApfSkudG8oMC42NSwgeyBzY2FsZTogMCB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KGNoYXJhY3Rlcikuc3RhcnQoKTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xlbmRHID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHZhciBwb3NWZW50MiA9IHRoaXMudmVudDIuZ2V0UG9zaXRpb24oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmVudDIuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbikucGxheSgnVmVudCcpO1xyXG4gICAgICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC41LCB7IHBvc2l0aW9uOiBjYy52Myhwb3NWZW50Mi54LCBwb3NWZW50Mi55LCAyMCkgfSwge2Vhc2luZzogJ2Vhc2VCYWNrSW4nfSkudG8oMC41LCB7IHNjYWxlOiA1MDAgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHR3ZWVuLnRhcmdldChjaGFyYWN0ZXIpLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuc2V0UG9zaXRpb24oY2MudjMocG9zVmVudDIueCArIDEwLCBwb3NWZW50Mi55IC0gNDAsIC0xNSkpO1xyXG4gICAgICAgICAgICAgICAgY2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNoYXJhY3Rlci56ID0gNTU7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuc2NhbGUgPSA1MDA7XHJcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuYW5nbGUgPSA4MDtcclxuICAgICAgICAgICAgICAgIGNoYXJhY3Rlci5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKC05MCwgMTgwLCAtMjQpO1xyXG4gICAgICAgICAgICB9LCAwLjgpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==