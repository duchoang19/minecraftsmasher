
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Enemy/EnemyController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6fb44ar/vdHCqZo6WpsF2GK', 'EnemyController');
// Scripts/Enemy/EnemyController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Random_1 = require("../Common/Random");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnemyController = /** @class */ (function (_super) {
    __extends(EnemyController, _super);
    function EnemyController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampTop = 0;
        _this.clampBottom = 0;
        _this.PointShoot = null;
        _this.bodyDeath = null;
        _this.effectBlood = null;
        _this.posBlood = null;
        _this.moveX = 0;
        _this.moveY = 0;
        _this.moveZ = 0;
        _this.degree = 0;
        _this.boolCheckAttacking = false;
        _this.boolCheckAttacked = false;
        _this.boolEnemyDeath = false;
        _this.checkFollowPlayer = false;
        _this.checkDeath = false;
        return _this;
    }
    EnemyController.prototype.onLoad = function () {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    };
    EnemyController.prototype.update = function () {
        var _this = this;
        if (!this.checkDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
                var maxDistance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolEnemyDeath) {
                            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.getComponent(CharacterController_1.default).LevelUpPlayer();
                            // this.DestroyByKatana();
                            this.scheduleOnce(function () {
                                _this.DestroyByHammer();
                            }, 0.1);
                            this.checkDeath = true;
                            Global_1.default.boolCheckAttacked = false;
                            Global_1.default.boolCheckAttacking = false;
                        }
                    }
                }
            }
        }
    };
    EnemyController.prototype.StartMove = function () {
        var _this = this;
        this.schedule(function () {
            _this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    };
    EnemyController.prototype.DestroyByHammer = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        // this.PointShoot.active = false;
        this.node.eulerAngles = new cc.Vec3(0, 0, this.node.eulerAngles.z);
        // this.node.scaleZ = 100;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 5);
    };
    EnemyController.prototype.DestroyByKatana = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
        this.node.stopAllActions();
        // this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
    };
    EnemyController.prototype.SpawnerBlood = function () {
        this.bodyDeath.active = true;
        var pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 2);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    };
    EnemyController.prototype.SpawnerEffectBlood = function (x, y, z) {
        var EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    };
    EnemyController.prototype.SpawnerBodyDeath = function (timing) {
        var _this = this;
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(function () {
            _this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            _this.node.destroy();
        }, timing);
    };
    EnemyController.prototype.moveEnemy = function () {
        var _this = this;
        if (this.node.name == 'EnemyHunter1') {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hunter_run");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
        }
        if (this.checkFollowPlayer) {
            var Distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
            var duration = Distance / 21;
            this.moveX = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            while (Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    };
    EnemyController.prototype.betweenDegree = function (dirVec, comVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    EnemyController.prototype.EnemyAttack = function () {
        var _this = this;
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(function () {
            _this.boolCheckAttacking = true;
            _this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(function () {
        }, 0.6);
        this.scheduleOnce(function () {
            Global_1.default.boolStartAttacking = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    };
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "PointShoot", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "bodyDeath", void 0);
    __decorate([
        property(cc.Prefab)
    ], EnemyController.prototype, "effectBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "posBlood", void 0);
    EnemyController = __decorate([
        ccclass
    ], EnemyController);
    return EnemyController;
}(cc.Component));
exports.default = EnemyController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcRW5lbXlcXEVuZW15Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFDbkUsK0RBQXdFO0FBQ3hFLDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRTVDO0lBQTZDLG1DQUFZO0lBRHpEO1FBQUEscUVBK01DO1FBM01HLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFHdEIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFHdkIsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUdyQixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsWUFBTSxHQUFXLENBQUMsQ0FBQztRQUNuQix3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLHVCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNuQyxnQkFBVSxHQUFZLEtBQUssQ0FBQzs7SUE0S2hDLENBQUM7SUExS0csZ0NBQU0sR0FBTjtRQUNJLDJEQUEyRDtJQUMvRCxDQUFDO0lBRUQsZ0NBQU0sR0FBTjtRQUFBLGlCQTBDQztRQXpDTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLElBQUksZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDeEQsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1TyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVPLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNU8sSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakwsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekwsSUFBSSxjQUFjLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsY0FBYyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxjQUFjLEdBQUcsY0FBYyxFQUFFO29CQUNqQyxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtxQkFDSTtvQkFDRCxRQUFRLEdBQUcsY0FBYyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbE8sSUFBSSxXQUFXLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsaUJBQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzTixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxFQUFFO29CQUM3QixJQUFJLFFBQVEsR0FBRyxXQUFXLEVBQUU7d0JBQ3hCLElBQUksZ0JBQU0sQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQ2xELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsYUFBYSxFQUFFLENBQUM7NEJBQ25ILDBCQUEwQjs0QkFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQ0FDZCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7NEJBQzNCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzs0QkFDUixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzs0QkFDdkIsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7NEJBQ2pDLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO3lCQUNyQztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7SUFDVCxDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNWLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEQsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25FLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4RCxDQUFDO0lBRUQsc0NBQVksR0FBWjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN2RSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDOUMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkQsV0FBVyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDN0MsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEIsVUFBaUIsTUFBYztRQUEvQixpQkFTQztRQVJHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDZixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQStCQztRQTlCRyxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLGNBQWMsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQztTQUNuRjthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7U0FDckY7UUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsTyxJQUFJLFFBQVEsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLEtBQUssR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEcsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbEcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDbkM7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEYsT0FBTyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUM3RyxJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNyRjtZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0RyxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQztZQUNyRixDQUFDLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELHVDQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQW1CQztRQWxCRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3ZFO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1IsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2xDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2RSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDO0lBek1EO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0M7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzt1REFDRTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3FEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7d0RBQ0c7SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt1REFDUztJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NEQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0RBQ1U7SUFHOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDTztJQXhCUixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBOE1uQztJQUFELHNCQUFDO0NBOU1ELEFBOE1DLENBOU00QyxFQUFFLENBQUMsU0FBUyxHQThNeEQ7a0JBOU1vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFJhbmRvbSBmcm9tIFwiLi4vQ29tbW9uL1JhbmRvbVwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVuZW15Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcExlZnQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFJpZ2h0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcEJvdHRvbTogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIFBvaW50U2hvb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYm9keURlYXRoOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgZWZmZWN0Qmxvb2Q6IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwb3NCbG9vZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgbW92ZVg6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWTogbnVtYmVyID0gMDtcclxuICAgIG1vdmVaOiBudW1iZXIgPSAwO1xyXG4gICAgZGVncmVlOiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbEVuZW15RGVhdGg6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGNoZWNrRm9sbG93UGxheWVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjaGVja0RlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8vIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuQm94Q29sbGlkZXIzRCkuZW5hYmxlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgJiYgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBwb3MxID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jaGlsZHJlblswXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcG9zMyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY2hpbGRyZW5bMl0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24xID0gY2MudjIocG9zMS54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MxLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24yID0gY2MudjIocG9zMi54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MyLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWdyZWUgPSBkaXJlY3Rpb24xLnNpZ25BbmdsZShkaXJlY3Rpb24yKTtcclxuICAgICAgICAgICAgICAgICAgICBkZWdyZWUgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcG9zRW5lbXkgPSBjYy52Mih0aGlzLm5vZGUueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgdGhpcy5ub2RlLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWdyZWVXaXRoUG9zMSA9IHBvc0VuZW15LnNpZ25BbmdsZShkaXJlY3Rpb24xKTtcclxuICAgICAgICAgICAgICAgICAgICBkZWdyZWVXaXRoUG9zMSA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhkZWdyZWVXaXRoUG9zMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MyID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZVdpdGhQb3MyKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVhbE5lZWQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MyID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZWdyZWVXaXRoUG9zMSA+IGRlZ3JlZVdpdGhQb3MyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBsZXQgZGlzdGFuY2UgPSBVdGlsaXR5Lkluc3RhbmNlKFV0aWxpdHkpLkRpc3RhbmNlKGNjLnYyKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSksIGNjLnYyKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSksIGNjLnYyKHBvczMueCwgcG9zMy55KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKE1hdGguYWJzKHJlYWxOZWVkKSA8IGRlZ3JlZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGlzdGFuY2UgPCBtYXhEaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tlZCAmJiAhdGhpcy5ib29sRW5lbXlEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkxldmVsVXBQbGF5ZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLkRlc3Ryb3lCeUthdGFuYSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5EZXN0cm95QnlIYW1tZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAwLjEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3RhcnRNb3ZlKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVFbmVteSgpO1xyXG4gICAgICAgIH0sIDYuNSwgY2MubWFjcm8uUkVQRUFUX0ZPUkVWRVIsIDAuMSk7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5SGFtbWVyKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICAvLyB0aGlzLlBvaW50U2hvb3QuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMCwgMCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIC8vIHRoaXMubm9kZS5zY2FsZVogPSAxMDA7XHJcbiAgICAgICAgdGhpcy5ub2RlLnogPSB0aGlzLm1vdmVaICsgMTtcclxuICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJsb29kKCk7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyRWZmZWN0Qmxvb2QodGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueiArIDUpO1xyXG4gICAgfVxyXG5cclxuICAgIERlc3Ryb3lCeUthdGFuYSgpIHtcclxuICAgICAgICB0aGlzLmJvb2xFbmVteURlYXRoID0gdHJ1ZTtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZFNjcmVhbSwgZmFsc2UpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQucGx1c0VuZW15KTtcclxuICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LnNjYWxlKTtcclxuICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJvZHlEZWF0aCgwLjcpO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckVmZmVjdEJsb29kKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIC8vIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgIH1cclxuXHJcbiAgICBTcGF3bmVyQmxvb2QoKSB7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBvc0Jsb29kLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguc2V0UG9zaXRpb24ocG9zLngsIHBvcy55LCAyKTtcclxuICAgICAgICB0aGlzLmJvZHlEZWF0aC5ldWxlckFuZ2xlcyA9IGNjLnYzKDkwLCAwLCB0aGlzLm5vZGUuZXVsZXJBbmdsZXMueik7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckVmZmVjdEJsb29kKHg6IG51bWJlciwgeTogbnVtYmVyLCB6OiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgRWZmZWN0Qmxvb2QgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmVmZmVjdEJsb29kKTtcclxuICAgICAgICBFZmZlY3RCbG9vZC5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBFZmZlY3RCbG9vZC54ID0geDtcclxuICAgICAgICBFZmZlY3RCbG9vZC55ID0geTtcclxuICAgICAgICBFZmZlY3RCbG9vZC56ID0geiArIDE7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckJvZHlEZWF0aCh0aW1pbmc6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguc2V0UG9zaXRpb24odGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueik7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguZXVsZXJBbmdsZXMgPSBjYy52MygtOTAsIDE4MCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYm9keURlYXRoLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikuc3RvcCgpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgIH0sIHRpbWluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgbW92ZUVuZW15KCkge1xyXG4gICAgICAgIGlmKHRoaXMubm9kZS5uYW1lID09ICdFbmVteUh1bnRlcjEnKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJjaGFyYWN0ZXJfYm9uZXN8aHVudGVyX3J1blwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9ydW5fMlwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY2hlY2tGb2xsb3dQbGF5ZXIpIHtcclxuICAgICAgICAgICAgbGV0IERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSkpO1xyXG4gICAgICAgICAgICBsZXQgZHVyYXRpb24gPSBEaXN0YW5jZSAvIDIxO1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54O1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVZID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55O1xyXG4gICAgICAgICAgICB0aGlzLmRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgLSA5MDtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oZHVyYXRpb24sIHsgcG9zaXRpb246IGNjLnYzKHRoaXMubW92ZVgsIHRoaXMubW92ZVksIHRoaXMubW92ZVopIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FbmVteUF0dGFjaygpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVggPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWSA9IFJhbmRvbS5JbnN0YW5jZShSYW5kb20pLlJhbmRvbVJhbmdlKHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICB3aGlsZSAoVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgPCAxMDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubW92ZVggPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubW92ZVkgPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wQm90dG9tLCB0aGlzLmNsYW1wVG9wKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmRlZ3JlZSA9IHRoaXMuYmV0d2VlbkRlZ3JlZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52Mih0aGlzLm1vdmVYLCB0aGlzLm1vdmVZKSkgKyA5MDtcclxuICAgICAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oNiwgeyBwb3NpdGlvbjogY2MudjModGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSwgdGhpcy5tb3ZlWikgfSkuY2FsbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiY2hhcmFjdGVyX2JvbmVzfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5ub2RlLnJ1bkFjdGlvbihjYy5yb3RhdGUzRFRvKDAuMiwgY2MudjMoLTkwLCAtMTgwLCAtdGhpcy5kZWdyZWUpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYmV0d2VlbkRlZ3JlZShkaXJWZWMsIGNvbVZlYykge1xyXG4gICAgICAgIGxldCBhbmdsZURlZyA9IE1hdGguYXRhbjIoZGlyVmVjLnkgLSBjb21WZWMueSwgZGlyVmVjLnggLSBjb21WZWMueCkgKiAxODAgLyBNYXRoLlBJO1xyXG4gICAgICAgIHJldHVybiBhbmdsZURlZztcclxuICAgIH1cclxuXHJcbiAgICBFbmVteUF0dGFjaygpIHtcclxuICAgICAgICB0aGlzLmJvb2xDaGVja0F0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15VmVub21cIikge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQW1vbmdVc19BdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSGFtbWVyIEF0dGFja1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0F0dGFja2luZyA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEF0dGFjaywgZmFsc2UpO1xyXG4gICAgICAgIH0sIDAuNSk7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgIH0sIDAuNik7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ19VU19pZGxlXCIpO1xyXG4gICAgICAgIH0sIDEpO1xyXG4gICAgfVxyXG5cclxufSJdfQ==