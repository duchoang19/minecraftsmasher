"use strict";
cc._RF.push(module, '47feeRjYpxKSKMq10SbyDma', 'VentCtrl');
// Scripts/Vent/VentCtrl.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var VentCtrl = /** @class */ (function (_super) {
    __extends(VentCtrl, _super);
    function VentCtrl() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.xMin = 0;
        _this.xMax = 0;
        _this.yMin = 0;
        _this.yMax = 0;
        _this.vent2 = null;
        _this.boolCheckTele = false;
        return _this;
    }
    VentCtrl.prototype.onLoad = function () { };
    VentCtrl.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
    };
    VentCtrl.prototype.update = function (dt) {
        var _this = this;
        var character = this.gameplayInstance.gameplay.MyCharacter;
        if (this.xMin < character.x && this.xMax > character.x && this.yMin < character.y && this.yMax > character.y && !Global_1.default.boolCheckTele) {
            Global_1.default.boolCheckTele = true;
            Global_1.default.enableAttack = false;
            Global_1.default.boolEnableTouch = false;
            character.getComponent(CharacterController_1.default).enabled = false;
            character.getComponent(cc.SkeletonAnimation).play('Among_US_idle');
            this.node.getComponent(cc.Animation).play('Vent');
            character.getComponent(CharacterController_1.default).JoystickFollow.active = false;
            character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
            var tween = new cc.Tween().to(0.3, { position: cc.v3(this.node.x + 2, this.node.y, this.node.z + 10) }).to(0.65, { scale: 0 });
            tween.target(character).start();
            this.scheduleOnce(function () {
                Global_1.default.boolendG = true;
                Global_1.default.boolStartAttacking = false;
                var posVent2 = _this.vent2.getPosition();
                _this.vent2.getComponent(cc.Animation).play('Vent');
                var tween = new cc.Tween().to(0.5, { position: cc.v3(posVent2.x, posVent2.y, 20) }, { easing: 'easeBackIn' }).to(0.5, { scale: 500 }).call(function () {
                });
                tween.target(character).start();
                character.setPosition(cc.v3(posVent2.x + 10, posVent2.y - 40, -15));
                character.getComponent(CharacterController_1.default).ArrowDirection.active = false;
                character.z = 55;
                character.scale = 500;
                character.angle = 80;
                character.eulerAngles = new cc.Vec3(-90, 180, -24);
            }, 0.8);
        }
    };
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "xMax", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMin", void 0);
    __decorate([
        property(cc.Integer)
    ], VentCtrl.prototype, "yMax", void 0);
    __decorate([
        property(cc.Node)
    ], VentCtrl.prototype, "vent2", void 0);
    VentCtrl = __decorate([
        ccclass
    ], VentCtrl);
    return VentCtrl;
}(cc.Component));
exports.default = VentCtrl;

cc._RF.pop();