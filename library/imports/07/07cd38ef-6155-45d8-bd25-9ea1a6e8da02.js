"use strict";
cc._RF.push(module, '07cd3jvYVVF2L0lnqGm6NoC', 'MineCraft1');
// Scripts/GamePlay/MC1/MineCraft1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../../Character/CharacterController");
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft1 = /** @class */ (function (_super) {
    __extends(MineCraft1, _super);
    function MineCraft1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.btnContinue = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.nStart = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.nEnemyEnd = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1._instance = _this;
        return _this;
    }
    MineCraft1_1 = MineCraft1;
    MineCraft1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft1.prototype.start = function () {
        // this.playGame();
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    MineCraft1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    MineCraft1.prototype.update = function (dt) {
        var _this = this;
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    MineCraft1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft1.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    // this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    MineCraft1.prototype.EndGame = function () {
        var _this = this;
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.nEndCard.active = true;
        this.playGame();
        this.Map.active = true;
        this.nEndCard.children[2].active = true;
        this.nEndCard.children[3].active = true;
        this.nEndCard.children[4].active = true;
        for (var i = 0; i < this.nEnemyEnd.childrenCount; i++) {
            this.nEnemyEnd.children[i].getComponent(EnemyController_1.default).StartMove();
        }
        this.MyCharacter.x = 0;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 12;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 0);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.scheduleOnce(function () {
            _this.nEndCard.children[3].active = false;
        }, 5);
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var MineCraft1_1;
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnContinue", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEndCard", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nStart", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft1.prototype, "nEnemyEnd", void 0);
    MineCraft1 = MineCraft1_1 = __decorate([
        ccclass
    ], MineCraft1);
    return MineCraft1;
}(Singleton_1.default));
exports.default = MineCraft1;

cc._RF.pop();