"use strict";
cc._RF.push(module, 'e56a2KIsLlOzqt3akB8DZIU', 'MineCraft2');
// Scripts/GamePlay/MC2/MineCraft2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../../Common/GamePlayInstance");
var Global_1 = require("../../Common/Global");
var KeyEvent_1 = require("../../Common/KeyEvent");
var Singleton_1 = require("../../Common/Singleton");
var EnemyController_1 = require("../../Enemy/EnemyController");
var MineCraft1_1 = require("../MC1/MineCraft1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MineCraft2 = /** @class */ (function (_super) {
    __extends(MineCraft2, _super);
    function MineCraft2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nightEnd = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nEndCard = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        MineCraft1_1.default._instance = _this;
        return _this;
    }
    MineCraft2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    MineCraft2.prototype.start = function () {
        this.playGame();
    };
    MineCraft2.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.EndGame();
        }
    };
    MineCraft2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    MineCraft2.prototype.EndGame = function () {
        var _this = this;
        // Global.boolendG = true;
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndCard.active = true;
            _this.nEndCard.runAction(cc.fadeIn(0.5));
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    MineCraft2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    MineCraft2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    MineCraft2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], MineCraft2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], MineCraft2.prototype, "nEndCard", void 0);
    MineCraft2 = __decorate([
        ccclass
    ], MineCraft2);
    return MineCraft2;
}(Singleton_1.default));
exports.default = MineCraft2;

cc._RF.pop();