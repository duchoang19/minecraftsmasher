const { ccclass, property } = cc._decorator;

@ccclass
export default class BloodController extends cc.Component {
    onLoad() {
        var tween = new cc.Tween().to(0.6, { scale: 500 });
        //tween.target(this.node.children[2]).start();
        tween.target(this.node).start();
    }
}