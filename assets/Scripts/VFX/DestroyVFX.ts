
const {ccclass, property} = cc._decorator;

@ccclass
export default class DestroyVFX extends cc.Component {

    @property(cc.Integer)
    timming: number = 0;
    start() {
        this.scheduleOnce(() => {
            this.node.destroy();
        }, this.timming);
    }
}
