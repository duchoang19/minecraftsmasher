import CharacterController from "../Character/CharacterController";
import GamePlayInstance, { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Random from "../Common/Random";
import Utility from "../Common/Utility";

const { ccclass, property } = cc._decorator;
@ccclass
export default class EnemyController extends cc.Component {

    @property(cc.Integer)
    clampLeft: number = 0;

    @property(cc.Integer)
    clampRight: number = 0;

    @property(cc.Integer)
    clampTop: number = 0;

    @property(cc.Integer)
    clampBottom: number = 0;

    @property(cc.Node)
    PointShoot: cc.Node = null;

    @property(cc.Node)
    bodyDeath: cc.Node = null;

    @property(cc.Prefab)
    effectBlood: cc.Prefab = null;

    @property(cc.Node)
    posBlood: cc.Node = null;

    moveX: number = 0;
    moveY: number = 0;
    moveZ: number = 0;
    degree: number = 0;
    boolCheckAttacking: boolean = false;
    boolCheckAttacked: boolean = false;
    boolEnemyDeath: boolean = false;
    checkFollowPlayer: boolean = false;
    checkDeath: boolean = false;

    onLoad() {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    }

    update() {
            if (!this.checkDeath) {
                if (Global.boolStartAttacking && Global.boolCheckAttacking) {
                    let pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.children[0].getPosition()));
                    let pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.children[1].getPosition()));
                    let pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.children[2].getPosition()));
                    let direction1 = cc.v2(pos1.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, pos1.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y);
                    let direction2 = cc.v2(pos2.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, pos2.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y);
                    let degree = direction1.signAngle(direction2);
                    degree = cc.misc.radiansToDegrees(degree);
                    let posEnemy = cc.v2(this.node.x - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, this.node.y - GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y);
                    let degreeWithPos1 = posEnemy.signAngle(direction1);
                    degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                    let degreeWithPos2 = posEnemy.signAngle(direction2);
                    degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                    let realNeed = 0;
                    degreeWithPos1 = Math.abs(degreeWithPos1);
                    degreeWithPos2 = Math.abs(degreeWithPos2);
                    if (degreeWithPos1 > degreeWithPos2) {
                        realNeed = degreeWithPos1;
                    }
                    else {
                        realNeed = degreeWithPos2;
                    }
                    let distance = Utility.Instance(Utility).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y));
                    let maxDistance = Utility.Instance(Utility).Distance(cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                    if (Math.abs(realNeed) < degree) {
                        if (distance < maxDistance) {
                            if (Global.boolCheckAttacked && !this.boolEnemyDeath) {
                                GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.getComponent(CharacterController).LevelUpPlayer();
                                // this.DestroyByKatana();
                                this.scheduleOnce(() => {
                                    this.DestroyByHammer();
                                }, 0.1);
                                this.checkDeath = true;
                                Global.boolCheckAttacked = false;
                                Global.boolCheckAttacking = false;
                            }
                        }
                    }
                }
            }
    }

    StartMove() {
        this.schedule(() => {
            this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    }

    DestroyByHammer() {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global.soundScream, false);
        instance.emit(KeyEvent.plusEnemy);
        instance.emit(KeyEvent.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        // this.PointShoot.active = false;
        this.node.eulerAngles = new cc.Vec3(0, 0, this.node.eulerAngles.z);
        // this.node.scaleZ = 100;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 5);
    }

    DestroyByKatana() {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global.soundScream, false);
        instance.emit(KeyEvent.plusEnemy);
        instance.emit(KeyEvent.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
        this.node.stopAllActions();
        // this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
    }

    SpawnerBlood() {
        this.bodyDeath.active = true;
        let pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 2);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    }

    SpawnerEffectBlood(x: number, y: number, z: number) {
        let EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    }

    SpawnerBodyDeath(timing: number) {
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(() => {
            this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            this.node.destroy();
        }, timing);
    }

    moveEnemy() {
        if(this.node.name == 'EnemyHunter1') {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hunter_run");
        } else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
        }
        if (this.checkFollowPlayer) {
            let Distance = Utility.Instance(Utility).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y));
            let duration = Distance / 21;
            this.moveX = GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance.Instance(GamePlayInstance).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(() => {
                this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random.Instance(Random).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random.Instance(Random).RandomRange(this.clampBottom, this.clampTop);
            while (Utility.Instance(Utility).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random.Instance(Random).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random.Instance(Random).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(() => {
                this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    }

    betweenDegree(dirVec, comVec) {
        let angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    }

    EnemyAttack() {
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(() => {
            this.boolCheckAttacking = true;
            this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(() => {
        }, 0.6);
        this.scheduleOnce(() => {
            Global.boolStartAttacking = false;
            this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    }

}