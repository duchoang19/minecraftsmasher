import Global from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundManager extends cc.Component {
    @property({
        type: cc.AudioClip
    })
    Bg: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    footStep: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Intro: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Attack: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Gate: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Scream: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Spin: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Reward: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    clickBtn: cc.AudioClip = null;

    @property({
        type: cc.AudioClip
    })
    katanaAttack: cc.AudioClip = null;

    onLoad() {
        Global.soundBG = this.Bg;
        Global.soundIntro = this.Intro;
        Global.soundFootStep = this.footStep;
        Global.soundAttack = this.Attack;
        Global.soundGate = this.Gate;
        Global.soundScream = this.Scream;
        Global.soundSpin = this.Spin;
        Global.soundReward = this.Reward;
        Global.soundClickBtn = this.clickBtn;
        Global.katanaAttack = this.katanaAttack;
    }
}
