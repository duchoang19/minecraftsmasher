// import Smasher1 from "../GamePlay/Smasher1,2/Smasher1";
// import Smasher5 from "../GamePlay/Smasher5/Smasher5";
import MineCraft1 from "../GamePlay/MC1/MineCraft1";
import Smasher1 from "../GamePlay/SS1/Smasher1";
import Singleton from "./Singleton";

const { ccclass, property } = cc._decorator;
export const instance = new cc.EventTarget();
@ccclass
export default class GamePlayInstance extends Singleton<GamePlayInstance> {
    gameplay: MineCraft1 = MineCraft1.Instance(MineCraft1);
    constructor() {
        super();
        GamePlayInstance._instance = this;
    }
}
