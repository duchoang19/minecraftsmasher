

import CharacterController from "../Character/CharacterController";
import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CameraFollow extends cc.Component {
    gameplayInstance: GamePlayInstance = null;
    cameraOffsetX: number = 0;
    cameraOffsetY: number = 0;
    cameraOffsetZ: number = 0;
    Target: cc.Node = null;
    //9,12
    plusY: number = 0;
    plusZ: number = 0;
    start() {
        this.gameplayInstance = GamePlayInstance.Instance(GamePlayInstance);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 43;
        this.cameraOffsetZ = this.node.z;
    }
    update() {
        if (Global.boolStartPlay && !Global.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if(Global.boolendG){
            cc.log('true')
            this.node.x = 0;
            this.node.y = -124;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38,0,0);
        }
        // else if (!Global.teleport) {
        //     Global.boolCheckTele = true;
        //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX + 20, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY + 20, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ - 50, 0.2);
        //     // this.node.x = -87;
        //     // this.node.y = 60;
        //     // this.node.z = 76.5;
        //     this.node.eulerAngles = new cc.Vec3(45.5, -2, 22);
        // }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    }
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    PlusYZ() {
        this.plusY += 12;
        this.plusZ += 9;
    }
}
