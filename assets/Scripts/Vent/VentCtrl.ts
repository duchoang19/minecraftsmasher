import CharacterController from "../Character/CharacterController";
import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class VentCtrl extends cc.Component {

    @property(cc.Integer)
    xMin: number = 0;

    @property(cc.Integer)
    xMax: number = 0;

    @property(cc.Integer)
    yMin: number = 0;

    @property(cc.Integer)
    yMax: number = 0;

    @property(cc.Node)
    vent2: cc.Node = null;

    gameplayInstance: GamePlayInstance;
    boolCheckTele: boolean = false;

    onLoad() { }

    start() {
        this.gameplayInstance = GamePlayInstance.Instance(GamePlayInstance);
    }

    update(dt) {
        let character = this.gameplayInstance.gameplay.MyCharacter;
        if (this.xMin < character.x && this.xMax > character.x && this.yMin < character.y && this.yMax > character.y && !Global.boolCheckTele) {
            Global.boolCheckTele = true;
            Global.enableAttack = false;
            Global.boolEnableTouch = false;
            character.getComponent(CharacterController).enabled = false;
            character.getComponent(cc.SkeletonAnimation).play('Among_US_idle');
            this.node.getComponent(cc.Animation).play('Vent');
            character.getComponent(CharacterController).JoystickFollow.active = false;
            character.getComponent(CharacterController).ArrowDirection.active = false;
            var tween = new cc.Tween().to(0.3, {position: cc.v3(this.node.x + 2, this.node.y, this.node.z + 10)}).to(0.65, { scale: 0 });
            tween.target(character).start();
            this.scheduleOnce(() => {
                Global.boolendG = true;
                Global.boolStartAttacking = false;
                var posVent2 = this.vent2.getPosition();
                this.vent2.getComponent(cc.Animation).play('Vent');
                var tween = new cc.Tween().to(0.5, { position: cc.v3(posVent2.x, posVent2.y, 20) }, {easing: 'easeBackIn'}).to(0.5, { scale: 500 }).call(() => {
                });
                tween.target(character).start();
                character.setPosition(cc.v3(posVent2.x + 10, posVent2.y - 40, -15));
                character.getComponent(CharacterController).ArrowDirection.active = false;
                character.z = 55;
                character.scale = 500;
                character.angle = 80;
                character.eulerAngles = new cc.Vec3(-90, 180, -24);
            }, 0.8)
        }
    }
}
