import CharacterController from "../Character/CharacterController";
import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class JoystickFollow extends cc.Component {

    @property(cc.Node)
    joyRing: cc.Node = null;

    @property(cc.Node)
    joyDot: cc.Node = null;

    stickPos: cc.Vec2 = null;
    touchLocation: cc.Vec2 = null;
    radius: number = 0;
    gameplayInstance: GamePlayInstance;

    onLoad () {
        this.radius = this.joyRing.width / 2;
    }

    start () {
        this.gameplayInstance = GamePlayInstance.Instance(GamePlayInstance);
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
    }

    touchStart(event) {
        if(!Global.boolendG && Global.boolStartPlay && !Global.boolStartAttacking) {
            var mousePos = event.getLocation();
            let localMousePos = this.node.convertToNodeSpaceAR(mousePos);
            this.node.opacity = 255;
            this.stickPos = localMousePos;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePos);
            this.joyDot.setPosition(localMousePos);
            this.gameplayInstance.gameplay.Guide.active = false;
            // this.gameplayInstance.gameplay.txtSmasher.active = false;
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).ArrowDirection.active = true;
        }
    }

    touchMove(event) {
        if(!Global.boolendG && Global.boolStartPlay && !Global.boolStartAttacking) {
            this.node.opacity = 255;
            Global.boolEnableTouch = true;
            if(!Global.boolFirstTouchJoyStick) {
                Global.boolFirstTouchJoyStick = true;
                if(this.gameplayInstance.gameplay.MyCharacter.name == 'MyDeadpool') {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_run");
                } else {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_run_2");
                }
                
            }
            if(this.touchLocation === event.getLocation()){
                return false;
            }

            this.gameplayInstance.gameplay.Guide.active = false;
            let touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            let distance = touchPos.mag();
            let posX = this.stickPos.x + touchPos.x;
            let posY = this.stickPos.y + touchPos.y;
            let p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global.touchPos = p;
            if(this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            } else {
                let x = this.stickPos.x + p.x * this.radius;
                let y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
        }
        this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).ArrowDirection.active = true;
    }

    touchCancel(event) {
        if (!Global.boolendG && Global.boolStartPlay && !Global.boolStartAttacking) {
            Global.boolEnableTouch = false;
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController).Attacking();
            this.node.opacity = 0;
            Global.boolFirstTouchJoyStick = false;
        }
    }
}
