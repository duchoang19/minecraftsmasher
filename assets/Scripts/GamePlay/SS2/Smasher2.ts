import CharacterController from "../../Character/CharacterController";
import { instance } from "../../Common/GamePlayInstance";
import Global from "../../Common/Global";
import KeyEvent from "../../Common/KeyEvent";
import Singleton from "../../Common/Singleton";
import EnemyController from "../../Enemy/EnemyController";
import VentCtrl from "../../Vent/VentCtrl";
import Smasher1 from "../SS1/Smasher1";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class Smasher2 extends Singleton<Smasher1> {

    @property(cc.Node)
    joyStickFollow: cc.Node = null;

    @property(cc.Node)
    Map: cc.Node = null;

    @property(cc.Integer)
    countEnemyEnd: number = 0;

    @property(cc.Node)
    MyCharacter: cc.Node = null;

    @property(cc.Node)
    Guide: cc.Node = null;

    @property(cc.Node)
    Effect: cc.Node = null;

    @property(cc.Node)
    enemyParent: cc.Node = null;

    @property(cc.Node)
    btnDownload: cc.Node = null;

    @property(cc.Node)
    txtEndGame: cc.Node = null;

    @property(cc.Node)
    txtStart: cc.Node = null;

    @property(cc.Node)
    btnAll: cc.Node = null;

    @property(cc.Node)
    arrow: cc.Node = null;

    @property(VentCtrl)
    vent1: VentCtrl = null;

    countEnemy: number = 0;
    boolCheckEnd: boolean = false;
    distance: cc.Node = null;

    boolcheckInteraction: boolean = false;
    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;

    constructor() {
        super();
        Smasher1._instance = this;
    }

    onEnable() {
        instance.on(KeyEvent.scale, this.scale, this);
        instance.on(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.on(KeyEvent.activeGuide, this.activeGuide, this);
    }

    onDisable() {
        instance.off(KeyEvent.scale, this.scale, this);
        instance.off(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.off(KeyEvent.activeGuide, this.activeGuide, this);
    }

    start() {
        this.playGame();
        this.vent1.enabled = false;
    }

    update(dt) {
        if (Global.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtStart.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }

        if (this.countEnemy == this.countEnemyEnd) {
            this.vent1.enabled = true;
            this.arrow.active = true;
        }

        if(Global.boolCheckTele) {
            this.EndGame();
            this.arrow.active = false;
        }
    }

    EndGame() {
        Global.boolendG = true;
        this.joyStickFollow.active = false;
        this.txtEndGame.active = true;
        this.btnAll.active = true;
        this.Guide.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    }

    playGame() {
        Global.boolStartPlay = true;
        for (let i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController).StartMove();
        }
    }

    scale() {
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    }

    plusEnemy() {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    }

    activeGuide() {
        if (Global.boolStartPlay)
            this.Guide.active = true;
    }
}

export function getPositionX() {
    throw new Error("Function not implemented.");
}

