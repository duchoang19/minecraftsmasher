import { ActionType } from "../Common/EnumDefine";
import GamePlayInstance, { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CharacterController extends cc.Component {

    @property(cc.Node)
    ArrowDirection: cc.Node = null;

    @property(cc.Node)
    JoystickFollow: cc.Node = null;

    @property({ type: cc.Enum(ActionType) })
    actionType: ActionType = ActionType.IDLE;

    @property(cc.Integer)
    speed: number = 250;

    @property(cc.Node)
    Weapon: cc.Node = null;

    @property(cc.Integer)
    timeAnim: number = 0;

    @property(cc.Node)
    placeBua: cc.Node = null;

    @property(cc.Prefab)
    effectSmoke: cc.Prefab = null;

    @property(cc.Integer)
    clampLeft: number = 0;

    @property(cc.Integer)
    clampRight: number = 0;

    @property(cc.Integer)
    clampBottom: number = 0;

    @property(cc.Integer)
    clampTop: number = 0;

    @property(cc.Integer)
    level: number = 0;

    @property(cc.Integer)
    scale: number = 0;

    rigidbody: cc.RigidBody3D;
    collider: cc.Collider3D;

    originalSpeed: number = 0;
    attack: boolean = false;

    gameplayInstance: GamePlayInstance = null;
    boolPlaySoundFoot: boolean = false;

    // onLoad () {}

    start () {
        this.gameplayInstance = GamePlayInstance.Instance(GamePlayInstance);
        Global.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        let physic_mamanger = cc.director.getPhysics3DManager();
        this.rigidbody.setLinearVelocity(new cc.Vec3(0,0, 0));
        physic_mamanger.enabled = true
        physic_mamanger.gravity = cc.v3(0, 0, -3000);
        this.collider = this.getComponent(cc.Collider3D);
    }

    update() {
        if(Global.boolEnableTouch && !Global.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            let PosForX = this.node.getPosition();
            let PosForY = this.node.getPosition();
            PosForX.addSelf(Global.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global.soundFootStep, false);
                this.scheduleOnce(() => {
                    this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global.touchPos.y, Global.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    }

    Attacking() {
        Global.boolStartAttacking = true;
        Global.boolCheckAttacking = false;
        // this.Hand.active = true;
        this.Weapon.active = true;

        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_attack");
        }

        // if(!Global.teleport) {
        //     cc.log('=======' , this.attack)
        //     this.attack = true;
        // } else {
        //     this.attack = false;
        // }

        this.scheduleOnce(() => {
            Global.boolCheckAttacking = true;
            Global.boolCheckAttacked = true;
            // this.spawnEffectSmoke(this.effectSmoke);
            if (this.node.name == "MyDeadpool") {
                cc.audioEngine.playEffect(Global.katanaAttack, false);
            } else {
                cc.audioEngine.playEffect(Global.soundAttack, false);
            }
        }, 0.5);

        // if(Global.teleport) {
        //     this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        // }

        this.scheduleOnce(() => {
            instance.emit(KeyEvent.activeGuide);
            Global.boolStartAttacking = false;
            
            if (this.node.name == "MyDeadpool") {
                this.node.getComponent(cc.SkeletonAnimation).play("character_bones|assassin_idle");
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("character_bones|hammer_idle");
            }
            // this.Hand.active = false;
            // this.Weapon.active = false;
        }, this.timeAnim);
    }

    LevelUpPlayer() {
        this.node.scaleX = this.node.scaleX + this.scale;
        this.node.scaleY = this.node.scaleY + this.scale;
        this.node.scaleZ = this.node.scaleZ + this.scale;
        if(this.level < 4)
            this.level++;
    }

    spawnEffectSmoke(smoke: cc.Prefab) {
        let smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        let pos = this.node.convertToWorldSpaceAR(this.placeBua.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    }
}
